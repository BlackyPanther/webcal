<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-13
// Description:
// Setup-File. Leads trough all installation steps
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
ini_set('display_errors', 'On');

// prepare vars
$url = str_replace($_SERVER['DOCUMENT_ROOT'], 'http://'.$_SERVER['HTTP_HOST'], __DIR__);
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") $url = str_replace("http", "https", $url);

// define some vars
define('main', true);
define('DIR', __DIR__.'/');
define('URL', $url.'/');

// include needed stuff
require_once DIR.'include/php/functions.php';
require_once DIR.'include/submodule/sql/sql.class.php';
require_once DIR.'include/php/Tx.class.php';
use AMWD\Tx as Tx;
use AMWD\SQL as SQL;

// load translations
Tx::LoadDirectory(DIR.'/include/lang/');

// stubs
$layout['title'] = Tx::T('WebCal.Title');
$layout['slogan'] = Tx::T('WebCal.Slogan');
$layout['headStyle'] =
$layout['navLeft'] =
$layout['navRight'] =
$layout['side'] =
$layout['content'] =
$layout['company'] =
$layout['companyLink'] = '';
$layout['javascript'] = array();
// will be overridden later on
$config['version'] = '2.0';

// check writable dir for config
is_writable(DIR.'include/') || die(Tx::T('WebCal.Installation.Step0.DirectoryNotWritable'));
class_exists('mysqli') || class_exists('SQLite3') || die(Tx::T('WebCal.Installation.Step0.DatabaseConnectorClassesMissing'));

// let's start!
switch ($_GET['step']) {
	case 1:
		$config['version'] = '2.0';
		$layout['content'] = nl2br(Tx::T('WebCal.Installation.Step1.Intro'));

		if (isset($_POST['action']) && $_POST['action'] == Tx::T('WebCal.Installation.Next')) {
			$cfg = file_get_contents(DIR.'include/config.example.php');

			$cfg = str_replace('{TYPE}', $_POST['type'], $cfg);

			$cfg = str_replace('{HOST}', empty($_POST['host']) ? 'localhost' : $_POST['host'], $cfg);
			$cfg = str_replace('{PORT}', empty($_POST['port']) ? '3306' : $_POST['port'], $cfg);
			$cfg = str_replace('{USER}', empty($_POST['user']) ? '' : $_POST['user'], $cfg);
			$cfg = str_replace('{PASS}', empty($_POST['pass']) ? '' : $_POST['pass'], $cfg);
			$cfg = str_replace('{BASE}', empty($_POST['base']) ? '' : $_POST['base'], $cfg);

			$cfg = str_replace('{PATH}', empty($_POST['path']) ? '' : $_POST['path'], $cfg);

			$cfg = str_replace('{PFX}', empty($_POST['pfx']) ? '' : $_POST['pfx'], $cfg);

			file_put_contents(DIR.'include/config.php', $cfg);
			header('Location: '.$_SERVER['PHP_SELF'].'?step=2');

		} else if (isset($_POST['action']) && $_POST['action'] == Tx::T('WebCal.Installation.Check')) {
			$error = false;

			if ($_POST['type'] == 'sqlite') {
				$dir = dirname($_POST['path']);
	
				if (!is_dir($dir)) {
					$layout['content'] .= jqAlert(Tx::T('WebCal.Installation.Step1.Error.DirectoryNotExists'), 'closethick', 'error');
					$error = true;
				}

				if (!is_writable($dir)) {
					$layout['content'] .= jqAlert(Tx::T('WebCal.Installation.Step1.Error.DirectoryNotWritable'), 'closethick', 'error');
					$error = true;
				}

				if (file_exists($_POST['path']) && !is_writable($_POST['path'])) {
					$layout['content'] .= jqAlert(Tx::T('WebCal.Installation.Step1.Error.FileNotWritable'), 'closethick', 'error');
					$error = true;
				}

				if (file_exists($_POST['path']) && is_writable($_POST['path']))
					$layout['content'] .= jqAlert(Tx::T('WebCal.Installation.Step1.Error.FileOverride'), 'alert');

				$layout['content'] .= '
				<form action="'.$_SERVER['PHP_SELF'].'?step=1" method="post">
				<table>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step1.Pfx').'</td>
						<td><input type="text" name="pfx" placeholder="webcal_" size="30" value="'.$_POST['pfx'].'" /></td>
					</tr>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step1.Path').':</td>
						<td><input type="text" name="path" placeholder="/absolute/path/to/file" size="30" value="'.$_POST['path'].'" /></td>
					</tr>
				</table>
				';

			} else {
				$sql = SQL::MySQL($_POST['user'], $_POST['pass'], $_POST['base'], $_POST['port'], $_POST['host']);

				$quer = array();
				$queries[] = "CREATE TABLE ".$_POST['pfx']."test
(
	testid int NOT NULL AUTO_INCREMENT,
	testentry varchar(100),
	PRIMARY KEY (testid)
);";
				$queries[] = "INSERT INTO ".$_POST['pfx']."test (testentry) VALUES ('This is an TESTentry');";
				$queries[] = "INSERT INTO ".$_POST['pfx']."test (testentry) VALUES ('Number Two');";
				$queries[] = "SELECT testentry FROM ".$_POST['pfx']."test WHERE testid = 2;";
				$queries[] = "DELETE FROM ".$_POST['pfx']."test WHERE testid = 1;";
				$queries[] = "DROP TABLE ".$_POST['pfx']."test;";

				try {
					$sql->open();

					foreach ($queries as $query) {
						$sql->query($query);
						if (!empty($sql->error()))
							throw new Exception($sql->error());
					}

					$sql->close();
				} catch (Exception $ex) {
					$layout['content'] .= jqAlert($ex->getMessage(), 'closethick', 'error');
					$error = true;
				}

				$layout['content'] .= '
				<form action="'.$_SERVER['PHP_SELF'].'?step=1" method="post">
				<table>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step1.Host').':</td>
						<td><input type="text" name="host" placeholder="localhost" size="30" value="'.$_POST['host'].'" /></td>
					</tr>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step1.Port').':</td>
						<td><input type="text" name="port" placeholder="3306" size="30" value="'.$_POST['port'].'" /></td>
					</tr>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step1.User').':</td>
						<td><input type="text" name="user" placeholder="webcal" size="30" value="'.$_POST['user'].'" /></td>
					</tr>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step1.Pass').':</td>
						<td><input type="text" name="pass" placeholder="PaSsWoRd" size="30" value="'.$_POST['pass'].'" /></td>
					</tr>
						<tr>
						<td>'.Tx::T('WebCal.Installation.Step1.DB').':</td>
						<td><input type="text" name="base" placeholder="webcal" size="30" value="'.$_POST['base'].'" /></td>
					</tr>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step1.Pfx').'</td>
						<td><input type="text" name="pfx" placeholder="webcal_" size="30" value="'.$_POST['pfx'].'" /></td>
					</tr>
				</table>
				';
			}

			$layout['content'] .= '
			<input type="hidden" name="type" value="'.$_POST['type'].'" />
			<a href="'.$_SERVER['PHP_SELF'].'?step=1" class="button">'.Tx::T('WebCal.Installation.Prev').'</a>
			';

			if ($error) {
				$layout['content'] .= '<input type="submit" name="action" value="'.Tx::T('WebCal.Installation.Check').'" class="button" />';
			} else {
				$layout['content'] .= '<input type="submit" name="action" value="'.Tx::T('WebCal.Installation.Next').'" class="button" />';
			}

			$layout['content'] .= '</form>';
		} else {
			$layout['content'] .= '
			<form action="'.$_SERVER['PHP_SELF'].'?step=1" method="post">
			<table>
				<tr>
					<td>'.Tx::T('WebCal.Installation.Step1.Type').':</td>
					<td>
						<label><input type="radio" name="type" value="mysql" checked="checked" /> MySQL</label>
						<label><input type="radio" name="type" value="sqlite" /> SQLite 3</label>
					</td>
				</tr>
				<tr class="mysql">
					<td>'.Tx::T('WebCal.Installation.Step1.Host').':</td>
					<td><input type="text" name="host" placeholder="localhost" size="30" /></td>
				</tr>
				<tr class="mysql">
					<td>'.Tx::T('WebCal.Installation.Step1.Port').':</td>
					<td><input type="text" name="port" placeholder="3306" size="30" /></td>
				</tr>
				<tr class="mysql">
					<td>'.Tx::T('WebCal.Installation.Step1.User').':</td>
					<td><input type="text" name="user" placeholder="webcal" size="30" /></td>
				</tr>
				<tr class="mysql">
					<td>'.Tx::T('WebCal.Installation.Step1.Pass').':</td>
					<td><input type="text" name="pass" placeholder="PaSsWoRd" size="30" /></td>
				</tr>
				<tr class="mysql">
					<td>'.Tx::T('WebCal.Installation.Step1.DB').':</td>
					<td><input type="text" name="base" placeholder="webcal" size="30" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Installation.Step1.Pfx').'</td>
					<td><input type="text" name="pfx" placeholder="webcal_" size="30" /></td>
				</tr>
				<tr class="sqlite">
					<td>'.Tx::T('WebCal.Installation.Step1.Path').':</td>
					<td><input type="text" name="path" placeholder="/absolute/path/to/file" size="30" value="'.DIR.'include/WebCal.sqlite" /></td>
				</tr>
			</table>
			<a href="'.$_SERVER['PHP_SELF'].'?step=0" class="button">'.Tx::T('WebCal.Installation.Prev').'</a>
			<input type="submit" name="action" value="'.Tx::T('WebCal.Installation.Check').'" class="button" />
			</form>

			<style type="text/css">
			.sqlite { display: none; }
			</style>

			<script type="text/javascript">
			$(function() {
				$("input[name=type]").click(function() {
					var val = $(this).val();
					if (val == "sqlite") {
						$(".sqlite").show();
						$(".mysql").hide();
					} else {
						$(".sqlite").hide();
						$(".mysql").show();
					}
				});
			});
			</script>
			';
		}
		break;

	case 2:
		include DIR.'include/config.php';
		if ($config['type'] == 'sqlite') {
			$sql = SQL::SQLite($config['path']);
			$schema = file_get_contents(DIR.'include/sql/schema_sqlite.sql');
		} else {
			$sql = SQL::MySQL($config['user'], $config['pass'], $config['base'], $config['port'], $config['host']);
			$schema = file_get_contents(DIR.'include/sql/schema_mysql.sql');
		}
		$schema = str_replace('{PFX}', $config['pfx'], $schema);
		
		if (($ret = $sql->restoreDump($schema, true)) !== true) {
			$layout['content'] = nl2br($ret);
		} else {
			$layout['content'] = nl2br(Tx::T('WebCal.Installation.Step2.Content'));
			$layout['content'].= '
			<br />
			<a href="'.$_SERVER['PHP_SELF'].'?step=1" class="button">'.Tx::T('WebCal.Installation.Prev').'</a>
			<a href="'.$_SERVER['PHP_SELF'].'?step=3" class="button">'.Tx::T('WebCal.Installation.Next').'</a>
			';
		}
		break;

	case 3:
		include DIR.'include/config.php';
		if ($config['type'] == 'sqlite') {
			$sql = SQL::SQLite($config['path']);
		} else {
			$sql = SQL::MySQL($config['user'], $config['pass'], $config['base'], $config['port'], $config['host']);
		}
		
		$layout['content'] = nl2br(Tx::T('WebCal.Installation.Step3.Intro'));
		
		if (isset($_POST['action']) && $_POST['action'] == Tx::T('WebCal.Installation.Check')) {
			$error = false;
			foreach ($_POST as $key => $val)
				$_POST[$key] = trim($val);

			if (empty($_POST['firstname'])) {
				$layout['content'] .= jqAlert(Tx::T('WebCal.Installation.Step3.Error.FirstnameEmpty'), 'closethick', 'error');
				$error = true;
			}
			if (empty($_POST['lastname'])) {
				$layout['content'] .= jqAlert(Tx::T('WebCal.Installation.Step3.Error.LastnameEmpty'), 'closethick', 'error');
				$error = true;
			}
			if (empty($_POST['email']) || strpos($_POST['email'], '@') === false) {
				$layout['content'] .= jqAlert(Tx::T('WebCal.Installation.Step3.Error.EmailWrong'), 'closethick', 'error');
				$error = true;
			}
			if (empty($_POST['pass'])) {
				$layout['content'] .= jqAlert(Tx::T('WebCal.Installation.Step3.Error.PasswordEmpty'), 'closethick', 'error');
				$error = true;
			}
			
			if ($error) {
				$layout['content'] .= '
				<form action="'.$_SERVER['PHP_SELF'].'?step=3" method="post">
				<table>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step3.Firstname').'</td>
						<td><input type="text" name="firstname" size="30" value="'.$_POST['firstname'].'" /></td>
					</tr>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step3.Lastname').'</td>
						<td><input type="text" name="lastname" size="30" value="'.$_POST['lastname'].'" /></td>
					</tr>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step3.Email').'</td>
						<td><input type="email" name="email" value="'.$_POST['email'].'" size="30" /></td>
					</tr>
					<tr>
					<td>'.Tx::T('WebCal.Installation.Step3.Password').'</td>
						<td><input type="password" name="pass" value="'.$_POST['pass'].'" size="30" /></td>
					</tr>
				</table>

				<a href="'.$_SERVER['PHP_SELF'].'?step=3" class="button">'.Tx::T('WebCal.Installation.Prev').'</a>
				<input type="submit" name="action" value="'.Tx::T('WebCal.Installation.Check').'" class="button" />
				</form>
				';
			} else {
				$layout['content'] .= '
				<form action="'.$_SERVER['PHP_SELF'].'?step=3" method="post">
				<table>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step3.Firstname').'</td>
						<td><input type="text" name="firstname" size="30" value="'.$_POST['firstname'].'" /></td>
					</tr>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step3.Lastname').'</td>
						<td><input type="text" name="lastname" size="30" value="'.$_POST['lastname'].'" /></td>
					</tr>
					<tr>
						<td>'.Tx::T('WebCal.Installation.Step3.Email').'</td>
						<td><input type="email" name="email" value="'.$_POST['email'].'" size="30" /></td>
					</tr>
					<tr>
					<td>'.Tx::T('WebCal.Installation.Step3.Password').'</td>
						<td><input type="password" name="pass" value="'.$_POST['pass'].'" size="30" /></td>
					</tr>
				</table>

				<a href="'.$_SERVER['PHP_SELF'].'?step=3" class="button">'.Tx::T('WebCal.Installation.Prev').'</a>
				<input type="submit" name="action" value="'.Tx::T('WebCal.Installation.Next').'" class="button" />
				</form>
				';
			}
			
		} else if (isset($_POST['action']) && $_POST['action'] == Tx::T('WebCal.Installation.Next')) {
			$queries = array();
			
			$queries[] = "INSERT INTO ".$config['pfx']."users (
    firstname
  , lastname
  , email
  , pass
  , admin
  , daybegin
  , dayend
)
VALUES (
    '".$_POST['firstname']."'
  , '".$_POST['lastname']."'
  , '".$_POST['email']."'
  , '".crypt($_POST['pass'], '$2a$07$'.randomString(20).'$')."'
  , 1
  , '".date('Y-m-d')." 08:00:00'
  , '".date('Y-m-d')." 18:00:00'
);";
			$queries[] = "INSERT INTO ".$config['pfx']."settings (daybegin,dayend)
VALUES ('".date('Y-m-d')." 06:00:00', '".date('Y-m-d')." 20:00:00');";

			$sql->open();
			foreach ($queries as $query)
				$sql->query($query);
			$sql->close();
			
			header('Location: '.$_SERVER['PHP_SELF'].'?step=4');
			exit;
		} else {
			$layout['content'] .= '
			<form action="'.$_SERVER['PHP_SELF'].'?step=3" method="post">
			<table>
				<tr>
					<td>'.Tx::T('WebCal.Installation.Step3.Firstname').'</td>
					<td><input type="text" name="firstname" size="30" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Installation.Step3.Lastname').'</td>
					<td><input type="text" name="lastname" size="30" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Installation.Step3.Email').'</td>
					<td><input type="email" name="email" placeholder="webmaster@'.$_SERVER['HTTP_HOST'].'" size="30" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Installation.Step3.Password').'</td>
					<td><input type="password" name="pass" placeholder="'.randomString().'" size="30" /></td>
				</tr>
			</table>
			
			<a href="'.$_SERVER['PHP_SELF'].'?step=2" class="button">'.Tx::T('WebCal.Installation.Prev').'</a>
			<input type="submit" name="action" value="'.Tx::T('WebCal.Installation.Check').'" class="button" />
			</form>
			';
		}
		break;

	case 4:
		include DIR.'include/config.php'; 
		$layout['content'] = nl2br(Tx::T('WebCal.Installation.Step4'));
		$layout['content'].= '
		<br />
		<a href="'.URL.'" class="button">'.Tx::T('WebCal.Installation.Next').'</a>';
		break;

	case 0:
	default:
		$layout['content'] = nl2br(Tx::T('WebCal.Installation.Step0.Content'));
		$layout['content'].= '<br /><br /><a href="'.$_SERVER['PHP_SELF'].'?step=1" class="button">'.Tx::T('WebCal.Installation.Next').'</a>';
		break;
}

$layout['content'] .= '
<script type="text/javascript">
	$(function() { $(".button").button(); });
</script>
';

include DIR.'include/php/layout.php';

?>