<?php
/*
// =============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-14
// Description:
// Default Site - creating the view of the calendar
// =============================================================================

// =============================================================================
// Changelog:
//
// Date       | Change
// -----------+-----------------------------------------------------------------
//            |
// =============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
use AMWD\Tx as Tx;

if (preg_match('/^(\d{4})-(?:0[1-9]|1[1-2])-(?:0[1-9]|1[0-9]|2[0-9]|3[0-1])$/', $_GET['day'])) {
	$_SESSION['day'] = $_GET['day'];
} else if (empty($_SESSION['day'])){
	$_SESSION['day'] = date('Y-m-d');
}

$allowedViews = array('day', 'week', 'month');
if (in_array($_GET['view'], $allowedViews)) {
	$_SESSION['View'] = $_GET['view'];
}

if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']) {
	$sql->open();
	$res = $sql->query("SELECT lastLogin, firstname, lastname FROM ".$config['pfx']."users WHERE userid = ".$_SESSION['uID']);
	$row = $sql->fetch_object($res);
	$sql->close();
	$row->name = $row->firstname.' '.$row->lastname;

	$layout['navLeft'] = Tx::T('WebCal.Menu.State.LoggedIn').': <a href="#" class="showUserSettings" title="'.Tx::T('WebCal.Menu.State.LastLogin').': '.date('d.m.Y H:i', strtotime($row->lastLogin)).'">'.$row->name.'</a><br />';
	$layout['navRight'] = '<a href="'.URL.'admin.php?p=logout">'.Tx::T('WebCal.Menu.Action.Logout').'</a><br />';
}
else {
	$layout['navLeft'] = Tx::T('WebCal.Menu.State.LoggedOut').'<br />';
	$layout['navRight'] = '<a href="'.URL.'admin.php?p=login">'.Tx::T('WebCal.Menu.Action.Login').'</a><br />';
}

$layout['navLeft'] .= Tx::T('WebCal.Menu.View.View').': <a href="#" id="viewCalDay">'.Tx::T('WebCal.Menu.View.Day').'</a> | <a href="#" id="viewCalWeek">'.Tx::T('WebCal.Menu.View.Week').'</a> | <a href="#" id="viewCalMonth">'.Tx::T('WebCal.Menu.View.Month').'</a>';
$layout['navRight'] .= '<a href="#" id="viewCalToday">'.Tx::T('WebCal.Menu.View.Today').'</a> | <a href="#" id="viewCalGoto">'.Tx::T('WebCal.Menu.View.Goto').'</a>';

$layout['content'] = '
<div id="calMenu">
	<div id="calMenuPrev" class="ui-state-default ui-corner-all" style="display: table-cell">
		<span class="ui-icon ui-icon-circle-triangle-w"></span>
	</div>
	
	<div id="calMenuNext" class="ui-state-default ui-corner-all" style="display: table-cell">
		<span class="ui-icon ui-icon-circle-triangle-e"></span>
	</div>
	
	<div id="calMenuTitle"></div>
	
	<div class="clear"></div>
</div>

<div id="calendar">
	<div id="calGrid"></div>
	<div id="calDates"></div>
</div>
';

?>