<?php
/*
// =============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-06-21
// Description:
// (Inter-)Actions with Database
// =============================================================================

// =============================================================================
// Changelog:
//
// Date       | Change
// -----------+-----------------------------------------------------------------
//            |
// =============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');

@ini_set('display_errors', 'on');
@error_reporting(E_ALL);
use AMWD\Tx as Tx;

function tx($key) {
	return Tx::T($key);
}

// deletes a date from database
// @param $date: object of date to delete; dateid and userid are important
// @returns: true on success, else exception
function dateDelete($date) {
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState'] && $_SESSION['uID'] == $date->userid) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$sql->open();
		$sql->query("DELETE FROM ".$config['pfx']."dates WHERE dateid = ".$date->dateid);
		$err = $sql->error();
		$sql->close();
		
		if (!empty($err)) {
			throw new Exception($err);
		} else {
			return true;
		}
	}
	
	throw new Exception(Tx::T('WebCal.Exceptions.Access'));
}

// save a date from edit
// @param $data: date object to save
// @returns: true on success, else exception
function dateSave($date) {
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState'] && $_SESSION['uID'] == $date->userid) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$begin = strtotime($date->begin);
		$end = strtotime($date->end);
		
		if ($end <= $begin) {
			throw new Exception(Tx::T('WebCal.Exceptions.DateTimesInvalid'));
		}
		
		if ($date->dateid == -1) {	// New Entry
			$query = "
INSERT INTO
	".$config['pfx']."dates
	(title, location, guest, begin, end, detail, public, created, userid)
VALUES
	('".$date->title."', '".$date->location."', '".$date->guest."', '".$date->begin."', '".$date->end."', '".$date->detail."', ".(($date->public) ? '1' : '0').", '".date('Y-m-d H:i:s')."', ".$date->userid.")";
		} else {										// existing Entry
			$query = "
UPDATE
	".$config['pfx']."dates
SET
	title = '".$date->title."',
	location = '".$date->location."',
	guest = '".$date->guest."',
	begin = '".$date->begin."',
	end = '".$date->end."',
	detail = '".$date->detail."',
	public = ".(($date->public) ? '1' : '0')."
WHERE
	dateid = ".$date->dateid;
		}
		
		$sql->open();
		$sql->query($query);
		$err = $sql->error();
		$sql->close();
	
		if (!empty($err)) {
			throw new Exception($err);
		} else {
			return true;
		}
	}
	
	throw new Exception(Tx::T('WebCal.Exceptions.Access'));
}

function dateForICS($dateid) {
	$config = $GLOBALS['config'];
	$sql = $GLOBALS['sql'];
	
	$query = "
SELECT
	dates.title AS summary,
	dates.begin,
	dates.end,
	dates.location,
	dates.detail AS description,
	dates.public,
	dates.userid,
	users.firstname,
	users.lastname,
	users.email
FROM
	".$config['pfx']."dates
LEFT JOIN
	".$config['pfx']."users using (userid)
WHERE
	dateid = ".$dateid;
	
	$sql->open();
	$res = $sql->query($query);
	$date = $sql->fetch_object($res);
	$sql->close();
	
	if ((isset($_SESSION['PublicAccess']) && $_SESSION['PublicAccess'] && $date->public)
			|| (isset($_SESSION['LoginState']) && $_SESSION['LoginState'] && ($date->public || $date->userid == $_SESSION['uID']))) {
		
		$url = str_replace($_SERVER['DOCUMENT_ROOT'], 'http://'.$_SERVER['HTTP_HOST'], DIR);
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") $url = str_replace("http", "https", $url);
		
		unset($date->public);
		unset($date->userid);
		
		$date->name = $date->firstname.' '.$date->lastname;
		unset($date->firstname);
		unset($date->lastname);
		
		$date->begin = strtotime($date->begin);
		$date->end = strtotime($date->end);
		$date->url = $url.'?day='.date('Y-m-d', $date->begin);
		
		return base64_encode(json_encode($date));
	} else {
		throw new Exception(Tx::T('WebCal.Exceptions.Access'));
	}
}

function sendCircularMail($email) {
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$query = "
SELECT
	userid,
	email
FROM
	".$config['pfx']."users";
		
		$emails = array();
		$sql->open();
		$res = $sql->query($query);
		while ($row = $sql->fetch_object($res)) {
			if (in_array($row->userid, $email->receiver)) {
				$emails[] = $row->email;
			}
		}
		$sql->close();
		
		$msg = Tx::T('WebCal.Dialogs.WriteMail.Timestamp').': '.date('Y-m-d H:i').PHP_EOL;
		$msg.= Tx::T('WebCal.Dialogs.WriteMail.Sender').': '.$email->senderName.PHP_EOL;
		$msg.= Tx::T('WebCal.Dialogs.WriteMail.Subject').': '.$email->subject.PHP_EOL;
		$msg.= '------------------------'.PHP_EOL;
		$msg.= $email->message;
		
		$from = $email->senderName.' <'.$email->senderMail.'>';
		
		$count = 0;
		for ($i = 0; $i < count($emails); $i++)
		{
			if (mail($emails[$i], $email->subject, $msg, "From: ".$from, "-f".$email->senderMail)) {
				$count++;
			}
		}
		
		return Tx::T('WebCal.Dialogs.WriteMail.EmailSent', array('COUNT' => $count));
	} else {
		throw new Exception(Tx::T('WebCal.Exceptions.Access'));
	}
}

function saveUserSettings($user) {
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$query = "
UPDATE
	".$config['pfx']."users
SET
	firstname = '".$user->firstname."',
	lastname = '".$user->lastname."',
	email = '".$user->email."',
	bgcolor = '".$user->bgcolor."',
	fcolor = '".$user->fcolor."',
	notify = '".$user->notify."',
	cal = '".$user->cal."',
	daybegin = '".date('Y-m-d')." ".$user->daybegin.":00',
	dayend = '".date('Y-m-d')." ".$user->dayend.":00',
	view = '".$user->view."'";
		
		if (!empty($user->password)) {
			$query .= ",
	pass = '".crypt($user->password, '$2a$07$'.randomString(20).'$')."'";
		}

		$query .= "
WHERE
	userid = ".$user->userid;
		
		$sql->open();
		$sql->query($query);
		$rows = $sql->affected_rows();
		$sql->close();
		
		return $rows == 1;
	} else {
		throw new Exception(Tx::T('WebCal.Exceptions.Access'));
	}
}

function saveSettings($settings) {
	if (isset($_SESSION['LoginState']) && isset($_SESSION['Admin'])
			&& $_SESSION['LoginState'] && $_SESSION['Admin'] == 1) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$query = "UPDATE
	".$config['pfx']."settings
SET
	title = '".$settings->title."',
	slogan = '".$settings->slogan."',
	company = '".$settings->company."',
	companyLink = '".$settings->companyLink."',
	daybegin = '".date('Y-m-d')." ".$settings->daybegin.":00',
	dayend = '".date('Y-m-d')." ".$settings->dayend.":00',
	defaultView = '".$settings->defaultView."',
	previewLimit = ".$settings->previewLimit.",
	publicAccess = ".($settings->publicAccess ? '1' : '0').",
	loading = ".($settings->loading ? '1' : '0').",
	showClock = ".($settings->showClock ? '1' : '0')."
WHERE
	settingsid = 1";
		
		$sql->open();
		$sql->query($query);
		$rows = $sql->affected_rows();
		$sql->close();
		
		return $rows == 1;
	} else {
		throw new Exception(Tx::T('WebCal.Exceptions.Access'));
	}
}

function createUser($user) {
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']
			&& isset($_SESSION['Admin']) && $_SESSION['Admin'] == 1) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$query = "SELECT COUNT(*) as count FROM ".$config['pfx']."users WHERE email = '".$user->email."'";
		
		$sql->open();
		$res = $sql->query($query);
		$count = $sql->fetch_object($res)->count;
		$sql->close();
		
		if (empty($user->email) || $count > 0)
			throw new Exception(Tx::T('WebCal.Sites.Admin.Users.EmailExists'));
		
		$salt = randomString(20); $pass = randomString(8);
		$crypt = crypt($pass, '$2a$07$'.$salt.'$');
		
		// rand for backgroundcolor
		$rand_r = mt_rand(0, 255);
		$rand_g = mt_rand(0, 255);
		$rand_b = mt_rand(0, 255);
		$bgcolor = (($rand_r < 16) ? '0' : '').dechex($rand_r).(($rand_g < 16) ? '0' : '').dechex($rand_g).(($rand_b < 16) ? '0' : '').dechex($rand_b);
		// rand for foreground text color
		$rand_r = mt_rand(0, 255);
		$rand_g = mt_rand(0, 255);
		$rand_b = mt_rand(0, 255);
		$fcolor = (($rand_r < 16) ? '0' : '').dechex($rand_r).(($rand_g < 16) ? '0' : '').dechex($rand_g).(($rand_b < 16) ? '0' : '').dechex($rand_b);
		
		$query = "INSERT INTO ".$config['pfx']."users
	(firstname, lastname, email, pass, bgcolor, fcolor, admin, daybegin, dayend)
VALUES
	('".$user->firstname."', '".$user->lastname."', '".$user->email."', '".$crypt."', '".$bgcolor."', '".$fcolor."', ".($user->admin ? '1' : '0').", '".date('Y-m-d')." 08:00:00', '".date('Y-m-d')." 18:00:00')";
		
		$sql->open();
		$sql->query($query);
		$rows = $sql->affected_rows();
		
		$return = ($rows == 1) ? true : $sql->error();
		
		$sql->close();
		
		return $return;
	} else {
		throw new Exception(Tx::T('WebCal.Exceptions.Access'));
	}
}

function loadEditUserSelect($user) {
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']
			&& isset($_SESSION['Admin']) && $_SESSION['Admin'] == 1) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$query = "SELECT
	userid,
	lastname,
	firstname
FROM
	".$config['pfx']."users
ORDER BY
	lastname, firstname, userid";
		
		$return = array();
		
		$sql->open();
		$res = $sql->query($query);
		while ($row = $sql->fetch_object($res)) {
			$return[] = '<option value="'.$row->userid.'">'.$row->lastname.', '.$row->firstname.'</option>';
		}
		$sql->close();
		
		return $return;
	} else {
		throw new Exception(Tx::T('WebCal.Exceptions.Access'));
	}
}

function deleteUser($userid) {
	if (isset($_SESSION['LoginState']) && isset($_SESSION['Admin'])
			&& $_SESSION['LoginState'] && $_SESSION['Admin'] == 1) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$query = "DELETE FROM
	".$config['pfx']."users
WHERE
	userid = ".$userid;
		
		$sql->open();
		$sql->query($query);
		$rows = $sql->affected_rows();
		
		$error = ($rows == 1) ? '' : $sql->error();
		$sql->close();
		
		if (empty($error))
			return true;
		
		throw new Exception($error);
	}
}

?>