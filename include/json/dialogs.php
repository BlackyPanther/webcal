<?php
/*
// =============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-06-10
// Description:
// HTML structure for Dialogboxes
// =============================================================================

// =============================================================================
// Changelog:
//
// Date       | Change
// -----------+-----------------------------------------------------------------
//            |
// =============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');

require_once DIR.'include/php/header.php';
use AMWD\Tx as Tx;

function dialogGoto() {
	return '
	<div id="dialogGoto" title="'.Tx::T('WebCal.Dialogs.Goto.Title').'">
		'.Tx::T('WebCal.Dialogs.Goto.Date').': <input id="gotoInput" type="text" placeholder="'.date('d.m.Y').'" size="10" />
	</div>
	';
}

function dialogDateAction($date) {
	$html = '
	<div id="dialogDateAction" title="'.$date->title.'">
		<table>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Begin').'</td>
				<td>'.date('d.m.Y H:i', strtotime($date->begin)).'</td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.End').'</td>
				<td>'.date('d.m.Y H:i', strtotime($date->end)).'</td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Location').'</td>
				<td>'.$date->location.'</td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Guest').'</td>
				<td>'.$date->guest.'</td>
			</tr>
			<tr>
				<td style="vertical-align: top;">'.Tx::T('WebCal.Dialogs.DateAction.Details').'</td>
				<td>'.$date->detail.'</td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Owner').'</td>
				<td>'.$date->lastname.', '.$date->firstname.'</td>
			</tr>
		</table>
		
		<button id="dialogDateActionExport">'.Tx::T('WebCal.Dialogs.DateAction.Export').'</button> 
		';
		
		if (isset($_SESSION['LoginState']) && $_SESSION['LoginState'] && $_SESSION['uID'] == $date->userid) {
			$placeholder = array();
			$placeholder['TITLE'] = $date->title;
			
			$html .= '
			<button id="dialogDateActionEdit">'.Tx::T('WebCal.Dialogs.DateAction.Edit').'</button> <button id="dialogDateActionDelete">'.Tx::T('WebCal.Dialogs.DateAction.Delete').'</button>
		
			<script type="text/javascript">
				$(function() {
					var date = JSON.parse("'.str_replace('"', '\"', json_encode($date)).'");
				
					$("#dialogDateActionEdit").click(function() {
						api("dialogDateEdit", date, function(response) {
							if (response.error === "") {
								$("body").append(response.data);
								$("#dialogDateEdit").dialog({
									width: 400,
									modal: true,
									close: function() {
										$(this).remove();
									}
								});
								$("#dialogDateAction").dialog("close");
							} else {
								console.log(response.error);
							}
						});
					});
				
					$("#dialogDateActionDelete").click(function() {
						var ok = confirm("'.Tx::T('WebCal.Dialogs.DateAction.ConfirmDelete').'");
						if (ok) {
							api("dateDelete", date, function(response) {
								if (response.error === "") {
									if (response.data) {
										loadCalendar();
										$("#dialogDateAction").dialog("close");
									} else {
										alert("'.Tx::T('WebCal.Dialogs.DateAction.DeleteFailed', $placeholder).'");
									}
								} else {
									console.log(response.error);
								}
							});
						}
					});
				});
			</script>
			';
		}
	$html .= '
		
		<script type="text/javascript">
			$(function() {
				$("#dialogDateActionExport").click(function() {
					api("dateForICS", '.$date->dateid.', function(res) {
						if (res.error === "") {
							var pos = window.location.href.lastIndexOf("/") + 1;
							var href = window.location.href.substr(0, pos);
							href += "include/php/ics.php?data=" + res.data;
							
							var site = window.open(href);
							site.focus();
							return false;
						} else {
							console.log(res.error);
						}
					});
				});
			});
		</script>
	</div>
	';
	
	return $html;
}

function dialogDateEdit($date) {
	$publicChecked = ($date->public) ? ' checked="checked"' : '';
	
	$html = '
	<div id="dialogDateEdit" title="'.Tx::T('WebCal.Dialogs.DateEdit.Title').'">
		<table>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateEdit.DateTitle').'</td>
				<td><input type="text" id="dateTitle" value="'.$date->title.'" size="30" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Location').'</td>
				<td><input type="text" id="dateLocation" value="'.$date->location.'" size="30" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Begin').'</td>
				<td><input type="text" id="dateBegin" value="'.date('d.m.Y H:i', strtotime($date->begin)).'" class="datetimepicker" placeholder="'.Tx::T('WebCal.Placeholder.DateTime').'" size="15" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.End').'</td>
				<td><input type="text" id="dateEnd" value="'.date('d.m.Y H:i', strtotime($date->end)).'" class="datetimepicker" placeholder="'.Tx::T('WebCal.Placeholder.DateTime').'" size="15" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Guest').'</td>
				<td><input type="text" id="dateGuest" value="'.$date->guest.'" size="30" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateEdit.Public').'</td>
				<td><label><input type="checkbox" id="datePublic"'.$publicChecked.' /> '.Tx::T('WebCal.Dialogs.DateEdit.PublicDesc').'</label></td>
			</tr>
			<tr>
				<td style="vertical-align: top;">'.Tx::T('WebCal.Dialogs.DateAction.Details').'</td>
				<td><textarea id="dateDetail" rows="3" cols="30">'.$date->detail.'</textarea></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Owner').'</td>
				<td><input type="text" readonly="readonly" value="'.$date->lastname.', '.$date->firstname.'" /></td>
			</tr>
		</table>
		';
		
		if (isset($_SESSION['LoginState']) && $_SESSION['LoginState'] && $_SESSION['uID'] == $date->userid) {
			$html .= '
			<button id="dateSave">'.Tx::T('WebCal.Dialogs.DateEdit.Save').'</button> <button id="dateAbort">'.Tx::T('WebCal.Dialogs.DateEdit.Cancel').'</button>
			
			<script type="text/javascript">
				$(function() {
					$(".datetimepicker").datetimepicker({
						dateFormat: "dd.mm.yy",
						timeFormat: "HH:mm"
					});

					$("#dateSave").click(function() {
						var save = {
							userid:   '.$date->userid.',
							dateid:   '.$date->dateid.',
							title:    $("#dateTitle").val(),
							location: $("#dateLocation").val(),
							begin:    dateFormat("Y-m-d H:i", $("#dateBegin").datetimepicker("getDate")) + ":00",
							end:      dateFormat("Y-m-d H:i", $("#dateEnd").datetimepicker("getDate")) + ":00",
							guest:    $("#dateGuest").val(),
							detail:   $("#dateDetail").val(),
							public:   $("#datePublic").is(":checked")
						};

						api("dateSave", save, function(response) {
							if (response.error === "") {
								if (response.data) {
									loadCalendar();
									$("#dialogDateEdit").dialog("close");
								} else {
									alert("'.Tx::T('WebCal.Dialogs.DateEdit.SaveFailed').'");
								}
							} else {
								console.log(response.error);
							}
						});
					});

					$("#dateAbort").click(function() {
						$("#dialogDateEdit").dialog("close");
					});
				});
			</script>
			';
		}
	$html .= '
	</div>
	';
	
	return $html;
}

function dialogNewDate() {
	$html = '
	<div id="dialogNewDate" title="'.Tx::T('WebCal.Dialogs.NewDate.Title').'">
		<table>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateEdit.DateTitle').'</td>
				<td><input type="text" id="dateTitle" size="30" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Location').'</td>
				<td><input type="text" id="dateLocation" size="30" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Begin').'</td>
				<td><input type="text" id="dateBegin" class="datetimepicker" placeholder="'.Tx::T('WebCal.Placeholder.DateTime').'" size="15" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.End').'</td>
				<td><input type="text" id="dateEnd" class="datetimepicker" placeholder="'.Tx::T('WebCal.Placeholder.DateTime').'" size="15" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateAction.Guest').'</td>
				<td><input type="text" id="dateGuest" size="30" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Dialogs.DateEdit.Public').'</td>
				<td><label><input type="checkbox" id="datePublic" /> '.Tx::T('WebCal.Dialogs.DateEdit.PublicDesc').'</label></td>
			</tr>
			<tr>
				<td style="vertical-align: top;">'.Tx::T('WebCal.Dialogs.DateAction.Details').'</td>
				<td><textarea id="dateDetail" rows="3" cols="30"></textarea></td>
			</tr>
		</table>
		
		<button id="saveNewDate">'.Tx::T('WebCal.Dialogs.NewDate.Save').'</button> <button id="dateAbort">'.Tx::T('WebCal.Dialogs.NewDate.Abort').'</button>
		
		<script type="text/javascript">
			$(function() {
				$("#dateAbort").click(function() {
					$("#dialogNewDate").dialog("close");
				});
				
				$("#saveNewDate").click(function() {
					// check for inputs
					var error = [];
					if ($("#dateTitle").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.NewDate.Error.TitleMissing').'";
					if ($("#dateBegin").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.NewDate.Error.BeginMissing').'";
					if ($("#dateEnd").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.NewDate.Error.EndMissing').'";
					
					// TODO: check dates for invalid elements
					
					if (error.length > 0) {
						alert("'.Tx::T('WebCal.Dialogs.NewDate.Error').':\n" + error.join("\n"));
					} else {
						// TODO: Eintrag speichern
						
						var save = {
							userid:   '.$_SESSION['uID'].',
							dateid:   -1,
							title:    $("#dateTitle").val(),
							location: $("#dateLocation").val(),
							begin:    dateFormat("Y-m-d H:i", $("#dateBegin").datetimepicker("getDate")) + ":00",
							end:      dateFormat("Y-m-d H:i", $("#dateEnd").datetimepicker("getDate")) + ":00",
							guest:    $("#dateGuest").val(),
							detail:   $("#dateDetail").val(),
							public:   $("#datePublic").is(":checked")
						};

						api("dateSave", save, function(response) {
							if (response.error === "") {
								if (response.data) {
									loadCalendar();
									$("#dialogNewDate").dialog("close");
								} else {
									alert("'.Tx::T('WebCal.Dialogs.DateEdit.SaveFailed').'");
								}
							} else {
								console.log(response.error);
							}
						});
					}
				});
			});
		</script>
	</div>
	';
	
	return $html;
}

function dialogWriteMail() {
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$query = "
SELECT
	userid,
	firstname,
	lastname,
	email
FROM
	".$config['pfx']."users
ORDER BY
	lastname ASC
";
		
		$sql->open();
		$res = $sql->query($query);
		
		$receiver = array(); $sender = array();
		while ($row = $sql->fetch_object($res)) {
			$receiver[] = '<option value="'.$row->userid.'">'.$row->lastname.', '.$row->firstname.'</option>';
			
			if ($row->userid == $_SESSION['uID'])
			{
				$sender['email'] = $row->email;
				$sender['name'] = $row->firstname.' '.$row->lastname;
			}
		}
		$sql->close();
		
		$html = '
		<div id="dialogWriteMail" title="'.Tx::T('WebCal.Dialogs.WriteMail.Title').'">
			<table>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.WriteMail.Sender').'</td>
					<td>'.$sender['name'].' &lt;'.$sender['email'].'&gt;</td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.WriteMail.Subject').'</td>
					<td><input type="text" id="mailSubject" size="40" /></td>
				</tr>
				<tr>
					<td style="vertical-align: top;">'.Tx::T('WebCal.Dialogs.WriteMail.Receiver').'</td>
					<td>
						<select id="mailReceiver" multiple="multiple" size="3">
							'.implode(PHP_EOL, $receiver).'
						</select>
					</td>
				</tr>
				<tr>
					<td style="vertical-align: top;">'.Tx::T('WebCal.Dialogs.WriteMail.Message').'</td>
					<td>
						<textarea id="mailMessage" cols="40" rows="8" placeholder="'.Tx::T('WebCal.Dialogs.WriteMail.MessagePlaceholder').'"></textarea>
					</td>
				</tr>
			</table>

			<button id="sendEmail">'.Tx::T('WebCal.Dialogs.WriteMail.Send').'</button> <button id="abortEmail">'.Tx::T('WebCal.Dialogs.WriteMail.Abort').'</button>
			
			<script type="text/javascript">
				$(function() {
					$("#abortEmail").click(function() {
						$("#dialogWriteMail").dialog("close");
					});
					
					$("#sendEmail").click(function() {
						var error = [];
						if ($("#mailSubject").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.WriteMail.Error.SubjectMissing').'";
						if ($("#mailMessage").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.WriteMail.Error.MessageMissing').'";
						if ($("#mailReceiver").val() === null) error[error.length] = "- '.Tx::T('WebCal.Dialogs.WriteMail.Error.ReceiverMissing').'";
						
						if (error.length > 0) {
							alert("'.Tx::T('WebCal.Dialogs.WriteMail.Error').'\n" + error.join("\n"));
							return;
						}
						
						var mail = {
							senderName: "'.$sender['name'].'",
							senderMail: "'.$sender['email'].'",
							subject: $("#mailSubject").val(),
							receiver: $("#mailReceiver").val(),
							message: $("#mailMessage").val()
						};
						
						api("sendCircularMail", mail, function(response) {
							if (response.error === "") {
								alert(response.data);
								$("#dialogWriteMail").dialog("close");
							}  {
								console.log(response.error);
							}
						});
					});
				});
			</script>
		</div>
		';

		return $html;
	} else {
		throw new Exception(Tx::T('WebCal.Exceptions.Access'));
	}
}

function dialogUserSettings($userid) {
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']) {
		$config = $GLOBALS['config'];
		$sql = $GLOBALS['sql'];
		
		$id = $_SESSION['uID'];
		
		if ($userid != null && isset($_SESSION['Admin']) && $_SESSION['Admin'] == 1)
			$id = $userid;
		
		$query = "
SELECT
	firstname,
	lastname,
	email,
	bgcolor,
	fcolor,
	notify,
	cal,
	daybegin,
	dayend,
	view,
	lastLogin
FROM
	".$config['pfx']."users
WHERE
	userid = ".$id;
		
		$sql->open();
		$res = $sql->query($query);
		$user = $sql->fetch_object($res);
		$sql->close();
		
		$privateChecked = $publicChecked = '';
		switch ($user->cal) {
			case 'private': $privateChecked = ' selected="selected"'; break;
			case 'public': $publicChecked = ' selected="selected"'; break;
		}
		
		$dayChecked = $weekChecked = $monthChecked = '';
		switch ($user->view) {
			case 'day': $dayChecked = ' selected="selected"'; break;
			case 'week': $weekChecked = ' selected="selected"'; break;
			case 'month': $monthChecked = ' selected="selected"'; break;
		}
		
		$html = '
		<div id="dialogUserSettings" title="'.Tx::T('WebCal.Dialogs.UserSettings.Title').'">
			<table>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.Firstname').'</td>
					<td><input type="text" id="userFirstname" size="30" value="'.$user->firstname.'" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.Lastname').'</td>
					<td><input type="text" id="userLastname" size="30" value="'.$user->lastname.'" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.Email').'</td>
					<td><input type="text" id="userEmail" size="30" value="'.$user->email.'" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.Password').'</td>
					<td><input type="password" id="userPassword" size="30" placeholder="'.Tx::T('WebCal.Dialogs.UserSettings.PasswordPlaceholder').'" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.Background').'</td>
					<td><input type="text" id="userBackground" size="30" value="'.$user->bgcolor.'" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.Fontcolor').'</td>
					<td><input type="text" id="userFontcolor" size="30" value="'.$user->fcolor.'" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.EntriesAtNotifyArea').'</td>
					<td><input type="number" id="userNotify" size="30" value="'.$user->notify.'" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.DefaultCalendar').'</td>
					<td>
						<select id="userCal">
							<option value="private"'.$privateChecked.'>'.Tx::T('WebCal.Side.View.Private').'</option>
							<option value="public"'.$publicChecked.'>'.Tx::T('WebCal.Side.View.Public').'</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.DayBegin').'</td>
					<td><input type="text" id="userDaybegin" size="30" value="'.date('H:i', strtotime($user->daybegin)).'" placeholder="'.Tx::T('WebCal.Placeholder.Time').'" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.DayEnd').'</td>
					<td><input type="text" id="userDayend" size="30" value="'.date('H:i', strtotime($user->dayend)).'" placeholder="'.Tx::T('WebCal.Placeholder.Time').'" /></td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.DefaultView').'</td>
					<td>
						<select id="userView">
							<option value="day"'.$dayChecked.'>'.Tx::T('WebCal.Menu.View.Day').'</option>
							<option value="week"'.$weekChecked.'>'.Tx::T('WebCal.Menu.View.Week').'</option>
							<option value="month"'.$monthChecked.'>'.Tx::T('WebCal.Menu.View.Month').'</option>
					</td>
				</tr>
				<tr>
					<td>'.Tx::T('WebCal.Dialogs.UserSettings.LastLogin').'</td>
					<td>'.date('d.m.Y H:i', strtotime($user->lastLogin)).'</td>
				</tr>
			</table>
			
			<button id="userSave">'.Tx::T('WebCal.Dialogs.UserSettings.Save').'</button> <button id="userAbort">'.Tx::T('WebCal.Dialogs.UserSettings.Abort').'</button>
			
			<script type="text/javascript">
				$(function() {
					$("#userBackground").colorpicker();
					$("#userFontcolor").colorpicker();
					$("#userDaybegin").timepicker({ "step": 15 });
					$("#userDayend").timepicker({ "step": 15 });
					
					$("#userAbort").click(function() {
						$("#dialogUserSettings").dialog("close");
					});
					
					$("#userSave").click(function() {
						var error = [];
						if ($("#userFirstname").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.UserSettings.Error.Firstname').'";
						if ($("#userLastname").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.UserSettings.Error.Lastname').'";
						if ($("#userEmail").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.UserSettings.Error.Email').'";
						if ($("#userBackground").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.UserSettings.Error.Background').'";
						if ($("#userFontcolor").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.UserSettings.Error.Fontcolor').'";
						if ($("#userNotify").val() == 0) error[error.length] = "- '.Tx::T('WebCal.Dialogs.UserSettings.Error.NumberNotify').'";
						if ($("#userDaybegin").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.UserSettings.Error.Daybegin').'";
						if ($("#userDayend").val().trim() === "") error[error.length] = "- '.Tx::T('WebCal.Dialogs.UserSettings.Error.Dayend').'";
						
						if (error.length > 0) {
							alert("'.Tx::T('WebCal.Dialogs.UserSettings.Error').'\n" + error.join("\n"));
						} else {
							var user = {
								userid:    '.$id.',
								firstname: $("#userFirstname").val(),
								lastname:  $("#userLastname").val(),
								email:     $("#userEmail").val(),
								password:  $("#userPassword").val().trim(),
								bgcolor:   $("#userBackground").val(),
								fcolor:    $("#userFontcolor").val(),
								notify:    $("#userNotify").val(),
								cal:       $("#userCal").val(),
								daybegin:  $("#userDaybegin").val().trim(),
								dayend:    $("#userDayend").val().trim(),
								view:      $("#userView").val()
							};
							
							api("saveUserSettings", user, function(response) {
								if (response.error === "") {
									if (response.data) {
										alert("'.Tx::T('WebCal.Dialogs.UserSettings.SaveSuccess').'");
									}
								} else {
									console.log(response.error);
								}
							});
						}
					});
				});
			</script>
		</div>
		';
		
		return $html;
	} else {
		throw new Exception(Tx::T('WebCal.Exceptions.Access'));
	}
}

?>