<?php
/*
// =============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-06-09
// Description:
// API functions for Calendar
// =============================================================================

// =============================================================================
// Changelog:
//
// Date       | Change
// -----------+-----------------------------------------------------------------
//            |
// =============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');

require_once DIR.'include/php/header.php';
use AMWD\Tx as Tx;

// build grid for defined View
// @param $view: define view, if null, session will be used
// @returns: HTML with grid
function calGrid($view) {
	$config = $GLOBALS['config'];
	$sql = $GLOBALS['sql'];
	$view = ($view == null) ? $_SESSION['View'] : $view;

	$sql->open();

	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']) {
		$query = "SELECT daybegin, dayend FROM ".$config['pfx']."users WHERE userid = ".$_SESSION['uID'];
	} else {
		$query = "SELECT daybegin, dayend FROM ".$config['pfx']."settings WHERE settingsid = 1";
	}

	$res = $sql->query($query);
	$row = $sql->fetch_object($res);

	$sql->close();

	$daybegin = strtotime($row->daybegin);
	$dayend = strtotime($row->dayend);

	switch ($view) {
		case "day":
			$out = '<ul class="viewDay">';

			$out .= '
			<li class="calHead">
				<div class="time"></div>
				<div class="day"></div>
			</li>
			';

			for ($i = date('G', $daybegin); $i < date('G', $dayend); $i++) {
				if ($i < 10) $i = '0'.$i;

				$out .= '
				<li>
					<div class="time">'.$i.':00</div>
					<div class="day">&nbsp;</div>
				</li>
				';
			}
			$out .= '</ul>';
			break;
		case "week":
			$out = '<ul class="viewWeek">';

			$out .= '
			<li class="calHead">
				<div class="time"></div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Monday').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Tuesday').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Wednesday').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Thursday').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Friday').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Saturday').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Sunday').'</div>
			</li>
			';

			for ($i = date('G', $daybegin); $i < date('G', $dayend); $i++) {
				if ($i < 10) $i = '0'.$i;

				$out .= '
				<li>
					<div class="time">'.$i.':00</div>
					<div class="day">&nbsp;</div>
					<div class="day">&nbsp;</div>
					<div class="day">&nbsp;</div>
					<div class="day">&nbsp;</div>
					<div class="day">&nbsp;</div>
					<div class="day">&nbsp;</div>
					<div class="day">&nbsp;</div>
				</li>
				';
			}

			$out .= "</ul>";
			break;
		case "month":
			$date = strtotime($_SESSION['day']);

			$days = date('t', $date);
			$first = strtotime('-'.(date('j', $date)-1).' days', $date);
			$last = strtotime('+'.($days - date('j', $date)).' days', $date);

			$out = '<ul class="viewMonth">';

			$out .= '
			<li class="calHead">
				<div class="time"></div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Mon').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Tue').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Wed').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Thu').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Fri').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Sat').'</div>
				<div class="day">'.Tx::T('WebCal.Sites.Calendar.Day.Sun').'</div>
			</li>
			';

			$day = 2 - date('N', $first);
			$weeks = date('W', $last) - date('W', $first);
			if ($weeks <= 0) {
				$weeks += date('W', $first);
			}

			for ($i = 0; $i <= $weeks; $i++) {
				$week = date('W', strtotime('+'.$i.' weeks', $first));
				
				$out .= '<li>';
				$out .= '<div class="time">'.$week.'</div>';
				
				for ($j = 0; $j < 7; $j++) {
					if (0 < $day && $day <= $days) {
						$out .= '<div class="day"><span class="date'.($day == date('j', $date) ? ' today' : '').'">'.($day < 10 ? '0' : '').$day.'</span></div>';
					} else {
						$out .= '<div class="day"><span class="date"></span></div>';
					}
					$day++;
				}
				
				$out .= '</li>';
			}
			$out .= '</ul>';
			break;
	}
	
	return $out;
}

// build defined head for grid View
// @param $view: defined view
// @returns: Text to corresponding grid
function calHead($view) {
	$view = ($view == null) ? $_SESSION['View'] : $view;
	$day = strtotime($_SESSION['day']);

	switch ($view) {
		case "day":
			return Tx::T('WebCal.Sites.Calendar.Day.'.date('l', $day)).', '.date('d.m.Y', $day);
		case "week":
			$weekstart = strtotime('-'.(date('N', $day)-1).' days', $day);
			$weekend = strtotime('+'.(7-date('N', $day)).' days', $day);
			return date('d.m.Y', $weekstart).' - '.date('d.m.Y', $weekend).' ('.Tx::T('WebCal.Sites.Calendar.Week').' '.date('W', $day).')';
		case "month":
			return Tx::T('WebCal.Sites.Calendar.Month.'.date('F', $day)).' '.date('Y', $day);
	}
}

// read out all dates and draw those in range
// @param $view: defined view, if null session will be used
// @returns: HTML with dates as blocks with positions
function calDates($view) {
	$view = ($view == null) ? $_SESSION['View'] : $view;
	$date = strtotime($_SESSION['day'].' 00:00:00');
	$out = new stdClass();
	$sql = $GLOBALS['sql'];
	$config = $GLOBALS['config'];
	
	$sql->open();

	// read begin and end of an day from settings
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']) {
		$query = "SELECT daybegin, dayend FROM ".$config['pfx']."users WHERE userid = ".$_SESSION['uID'];
	} else {
		$query = "SELECT daybegin, dayend FROM ".$config['pfx']."settings WHERE settingsid = 1";
	}

	$res = $sql->query($query);
	$row = $sql->fetch_object($res);
	$daybegin = strtotime($row->daybegin);
	$dayend = strtotime($row->dayend);
	
	$public = (isset($_SESSION['Calendar']) && $_SESSION['Calendar'] == 'private') ? 0 : 1;
	
	$query = "SELECT
		dates.dateid, dates.title, dates.location, dates.begin, dates.end,
		users.userid, users.firstname, users.lastname, users.fcolor, users.bgcolor
	FROM
		".$config['pfx']."dates
	LEFT JOIN
		".$config['pfx']."users using (userid)
	WHERE
		public = ".$public."
	ORDER BY
		begin ASC";
	
	$res = $sql->query($query);
	$html = '';
	
	switch ($view) {
		case "day":
			$begin = strtotime(date('Y-m-d', $date).' '.date('H:i', $daybegin).':00');
			$end = strtotime(date('Y-m-d', $date).' '.date('H:i', $dayend).':00');

			while ($row = $sql->fetch_object($res)) {
				$dbegin = strtotime($row->begin);
				$dend = strtotime($row->end);
				// skip date if end of it is before the beginning of our timespan
				// or if begin of it is after our timespan
				if ($dend <= $begin || $end <= $dbegin) {
					continue;
				}
				
				// date has something to do with our timespan... lets check it!
				$top = 0; $bottom = 0;
				$row->name = $row->lastname.', '.$row->firstname;
				$borderTop = ''; $borderBottom = '';
				
				if ($begin < $dbegin) {
					// Date starts later than the DayView
					$top = (date('G', $dbegin) * 60 + date('i', $dbegin)) - (date('G', $begin) * 60 + date('i', $begin));
					$borderTop = ' dateBorderTop';
				}
				
				// Date ends earlier than the DayView
				if ($dend < $end) {
					$bottom = (date('G', $end) * 60 + date('i', $end)) - (date('G', $dend) * 60 + date('i', $dend));
					$borderBottom = ' dateBorderBottom';
				}
				
				$html .= '
				<div class="date dateDay'.$borderTop.$borderBottom.'" style="top: '.$top.'px; bottom: '.$bottom.'px; background-color: #'.$row->bgcolor.'; color: #'.$row->fcolor.';" dateid="'.$row->dateid.'">
					<div class="title">'.$row->title.'</div>
					<div class="location">'.$row->location.'</div>
					<div class="owner">'.$row->name.'</div>
				</div>
				';
			}
			
			$out->html = $html;
			break;
		case "week":
			$begin = strtotime('-'.(date('N', $date)-1).' days', strtotime(date('Y-m-d', $date).' '.date('H:i', $daybegin).':00'));
			$end = strtotime('+'.(7-date('N', $date)).' days', strtotime(date('Y-m-d', $date).' '.date('H:i', $dayend).':00'));
			
			$width = 104;
			
			while ($row = $sql->fetch_object($res)) {
				$dbegin = strtotime($row->begin);
				$dend = strtotime($row->end);
				// skip date if end of it is before the beginning of our timespan
				// or if begin of it is after our timespan
				if ($dend <= $begin || $end <= $dbegin) {
					continue;
				}
				
				$dayCount = date('j', $dend) - date('j', $dbegin) + 1;
				$left = (date('N', $dbegin) - ($dbegin < $begin ? 8 : 1)) * $width;
				
				for ($i = 0; $i < $dayCount; $i++) {
					// For each day, that this date will need
					$top = 0; $bottom = 0;
					$title = ''; $location = '';
					$borderTop = ''; $borderBottom = '';
					
					if ($i == 0) { // BEGINN of date
						$top = (date('G', $dbegin) * 60 + date('i', $dbegin)) - (date('G', $begin) * 60 + date('i', $begin));
						$title = $row->title;
						$location = $row->location;
						$borderTop = ' dateBorderTop';
					}
					
					if ($i == $dayCount-1) { // END of date
						$bottom = (date('G', $end) * 60 + date('i', $end)) - (date('G', $dend) * 60 + date('i', $dend));
						$borderBottom = ' dateBorderBottom';
					}
					
					$html .= '
					<div class="date dateWeek'.$borderTop.$borderBottom.'" style="top: '.$top.'px; bottom: '.$bottom.'px; left: '.$left.'px; background-color: #'.$row->bgcolor.'; color: #'.$row->fcolor.';" dateid="'.$row->dateid.'">
						<div class="title">'.$title.'</div>
						<div class="location">'.$location.'</div>
					</div>
					';
					
					$left += $width;
				}
			}
			
			$out->html = $html;
			
			break;
		case "month":
			$mbegin = strtotime(date('Y-m', $date).'-01 00:00:00');
			$mend = strtotime(date('Y-m', $date).'-'.date('t', $date).' 23:59:59');
			
			$width = 104;
			$dateList = array();
			
			while ($row = $sql->fetch_object($res)) {
				$dbegin = strtotime($row->begin);
				$dend = strtotime($row->end);
				// skip date if end of it is before the beginning of our timespan
				// or if begin of it is after our timespan
				if ($dend <= $mbegin || $mend <= $dbegin) {
					continue;
				}
				
				$dateList[] = $row;
			}
			
			$day = 2 - date('N', $mbegin);
			$days = date('t', $date);
			$weeks = date('W', $mend) - date('W', $mbegin);
			if ($weeks <= 0) {
				$weeks += date('W', $mbegin);
			}

			for ($i = 0; $i <= $weeks; $i++) {
				$top = $i * $width;
				for ($j = 0; $j < 7; $j++) {
					if (0 < $day && $day <= $days) {
						$begin = strtotime(date('Y-m', $date).'-'.$day.' 00:00:00');
						$end = strtotime(date('Y-m', $date).'-'.$day.' 23:59:59');
						
						$left = $j * $width;
						
						$html .= '<ul class="dateList" style="position: absolute; top: '.$top.'px; left: '.$left.'px;">';
						
						foreach ($dateList as $el) {
							$dbegin = strtotime($el->begin);
							$dend = strtotime($el->end);
							
							if ($dend >= $begin && $end >= $dbegin)
							{
								$desc = $el->title.(empty($el->location) ? '' : ' ('.$el->location.')').', '.$el->firstname.' '.$el->lastname;
								$html .= '<li style="background: #'.$el->bgcolor.'; color: #'.$el->fcolor.';" title="'.$desc.'" dateid="'.$el->dateid.'">'.$el->title.'</li>';
							}
						}
						
						$html .= '</ul>';
					}

					$day++;
				}
			}
			
			$out->html = $html;
			break;
	}
	
	$sql->close();
	
	$out->width = '728px';
	$out->height = (((date('G', $dayend) * 60) + date('j', $dayend)) - ((date('G', $daybegin) * 60) + date('j', $daybegin))).'px';
	return $out;
}

// get details to an dateid
// @param $dateid: id of entry in database
// @returns: object with details
function calGetDate($dateid) {
	$sql = $GLOBALS['sql'];
	$config = $GLOBALS['config'];
	
	$query = "SELECT
		dates.*,
		users.userid, users.firstname, users.lastname
	FROM
		".$config['pfx']."dates
	LEFT JOIN
		".$config['pfx']."users using (userid)
	WHERE
		dateid = ".$dateid;
	
	$sql->open();
	$res = $sql->query($query);
	$obj = $sql->fetch_object($res);
	$sql->close();
	
	return $obj;
}


?>