<?php
/*
// =============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-14
// Description:
// Default Site - creating the view of the calendar
// =============================================================================

// =============================================================================
// Changelog:
//
// Date       | Change
// -----------+-----------------------------------------------------------------
//            |
// =============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');

function session($data) {
	$readonly = array('uID', 'Admin', 'LoginState', 'PublicAccess');
	
	if (isset($_SESSION[$data->key]) && isset($data->value) && !in_array($data->key, $readonly)) {
		$_SESSION[$data->key] = $data->value;
	}
	
	if ($data->key == '}{')
		return print_r($_SESSION, 1);
	
	return isset($_SESSION[$data->key]) ? $_SESSION[$data->key] : null;
}

?>