<?php
/*
// =============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-06-09
// Description:
// API Interface for all ajax requests.
// =============================================================================

// =============================================================================
// Changelog:
//
// Date       | Change
// -----------+-----------------------------------------------------------------
//            |
// =============================================================================
*/

// define as main
define('main', true);
define('DIR', str_replace('include/json', '', __DIR__));
require_once DIR.'include/php/header.php';

// include all api functions
include DIR.'include/json/session.php';
include DIR.'include/json/cal.php';
include DIR.'include/json/dialogs.php';
include DIR.'include/json/actions.php';

$params = json_decode(file_get_contents('php://input'));
$response = new stdClass();
$response->error = '';
$response->data = '';

if ($params->func == 'session'
		|| (isset($_SESSION['PublicAccess']) && $_SESSION['PublicAccess'])
		|| (isset($_SESSION['LoginState']) && $_SESSION['LoginState'])) {
	if (function_exists($params->func)) {
		try {
			$response->data = call_user_func($params->func, $params->data);
		} catch (Exception $ex) {
			$response->error = $ex->getMessage();
		}
	} else {
		$response->error = 'Function "'.$params->func.'" does not exist';
	}
} else {
	$response->error = 'unauthorized access';
}

header('Content-Type: application/json;charset=UTF-8');
echo json_encode($response);

?>