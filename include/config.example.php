<?php
/*
// ==============================================================================
// Author: WebCal Installation by Andreas Mueller <webmaster@am-wd.de>
// Created: {DATE}
// Description:
// config file. Contains configuration data for Database etc.
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');

$config['type'] = '{TYPE}';

$config['host'] = '{HOST}';
$config['port'] = {PORT};
$config['user'] = '{USER}';
$config['pass'] = '{PASS}';
$config['base'] = '{BASE}';

$config['path'] = '{PATH}';

$config['pfx'] = '{PFX}';

$config['version'] = '2.0';
$GLOBALS['config'] = $config;

?>