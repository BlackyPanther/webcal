/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-09
// Description:
// Default JavaScript File. Contains all necessary Methods
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/

// function to set the clock in Pagehead
function setClock() {
	var date = new Date();
	var min = date.getMinutes();
	var hrs = date.getHours();
	var day = date.getDate();
	var mon = date.getMonth()+1;
	var yrs = date.getFullYear();
	
	if (min < 10) min = '0'+min;
	if (hrs < 10) hrs = '0'+hrs;
	if (day < 10) day = '0'+day;
	if (mon < 10) mon = '0'+mon;
	
	$('#clkTime').text(hrs + ':' + min);
	$('#clkDate').text(day + '.' + mon + '.' + yrs);
}

// call API Interface
// @param func: function name to be called
// @param obj: object with all needed data
// @param json: true if response is json object, else false
// @returns: response from ajax call
function api(func, obj, callback, async, global) {
	if (async === undefined) async = true;
	if (global === undefined) global = false;
	
	var req = {};
	req.func = func;
	req.data = obj;

	return $.ajax({
		url: 'include/json/api.php',
		type: 'post',
		data: JSON.stringify(req),
		async: async,
		global: global,
		contentType: 'application/json',
		success: function(response) {
			if (typeof callback === "function") {
				callback(response);
			} else {
				console.log("API | callback function not defined");
			}
		},
		error: function(response) {
			console.log("API | execution not successful");
		}
	});
}

// extracts GET parameters from URL
// if key is not present, an KvP-Array will be returned
// if key doen't exist or error in URL occurred, null is returned
function $_GET(key) {
	var url = window.location.href;
	var tmp = url.split('?');
	
	if (tmp.length > 2) {
		console.log('Error in URL');
		return null;
	} else if (tmp.length == 1) {
		//console.log('No Parameters in URL')
		return null;
	}
	
	var seq = tmp[1].split('&');
	var params = [];
	seq.forEach(function(el, idx, ar) {
		params[idx] = {};
		
		tmp = el.split('=');
		params[idx].key = tmp[0];
		if (tmp[1] !== undefined)
			params[idx].value = tmp[1];
	});
	
	if (key === undefined || typeof key != 'string') {
		return params;
	} else {
		var ret = '';
		params.forEach(function(el, idx, ar) {
			if (el.key == key) {
				ret = (el.value === undefined) ? '' : el.value;
				return;
			}
		});
		return ret;
	}
}

// tries to read value from PHP Session variablen
// @param key: name of session variablen
// @param value: if used, the sessionvariable 'key' is set to value
function $_SESSION(key, value) {
	var req = {};
	req.key = key;
	
	if (value !== undefined)
		req.value = value;
	
	res = '';
	api('session', req, function(response) {
		if (response.error === '') {
			res = response.data;
		} else {
			console.log(response.error);
			res = null;
		}
	}, false);
	
	return res;
}

// all other jQuery handling
$(function() {
	// defaults at init
	setClock();
	setInterval(function() { setClock(); }, 1000);

	$(document).on('click', 'a', function() {
		if ($(this).attr('href') == '#')
			return false;
	});

	// adjust javascript elements
	$('#javascriptAlert').hide();
	$('.button').button();
	$('#calSelect').buttonset();
	$('#calDates').tooltip();
	$('.datetimepicker').datetimepicker({
		dateFormat: 'dd.mm.yy',
		timeFormat: 'HH:mm'
	});

	// activate mini calendar
	$('#sideCal').datepicker({
		inline: true,
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		showWeek: true,
		defaultDate: new Date($_SESSION('day')),
		dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
		monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
		onSelect: function(dateText, obj) {
			$_SESSION('day', dateText);
			loadCalendar();
		}
	});
	
	// update cal type
	$('input[name=calSelect]').change(function() {
		$_SESSION('Calendar', $(this).val());
		loadCalendar('dates');
	});
	
	$('#writeMail').click(function() {
		api('dialogWriteMail', null, function(response) {
			if (response.error === '') {
				$('body').append(response.data);
				$('#dialogWriteMail').dialog({
					width: 500,
					modal: true,
					close: function() {
						$(this).remove();
					}
				});
			} else {
				console.log(response.error);
			}
		});
		
		return false;
	});
	
	$('.showUserSettings').click(function() {
		api('dialogUserSettings', null, function(response) {
			if (response.error === '') {
				$('body').append(response.data);
				$('#dialogUserSettings').dialog({
					width: 500,
					modal: true,
					close: function() {
						$(this).remove();
					}
				});
			} else {
				console.log(response.error);
			}
		});
		
		return false;
	});
});
