/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-07-16
// Description:
// JavaScript functions and handling for general settings
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/

$(function() {
	$('#settingsDaybegin').timepicker({ 'step': 15 });
	$('#settingsDayend').timepicker({ 'step': 15 });
	
	$('#settingsSave').click(function() {
		var settings = {
			title:           $('#settingsTitle').val().trim(),
			slogan:          $('#settingsSubtitle').val().trim(),
			company:         $('#settingsCompany').val().trim(),
			companyLink:     $('#settingsCompanyLink').val().trim(),
			daybegin:        $('#settingsDaybegin').val().trim(),
			dayend:          $('#settingsDayend').val().trim(),
			defaultView:     $('#settingsView').val(),
			previewLimit:    $('#settingsPreviewLimit').val(),
			//attachement:     $('#settingsAttachement').is(':checked'),
			//attachementSize: $('#settingsAttachementSize').val(),
			publicAccess:    $('#settingsPublicAccess').is(':checked'),
			loading:         $('#settingsLoading').is(':checked'),
			showClock:       $('#settingsClock').is(':checked')
		};
		
		api('saveSettings', settings, function(response) {
			if (response.error === '') {
				if (response.data) {
					$('#settingsSaveSuccess').show();
					setTimeout(function() {
						$('#settingsSaveSuccess').fadeOut();
					}, 5000);
				}
			} else {
				console.log(response.error);
			}
		});
	});
});