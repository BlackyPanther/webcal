/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-07-16
// Description:
// Enable overlay for ajax requests
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/

// ajax binds for loading overlay
var requests = 0;
$(document).bind('ajaxSend', function() {
	requests++;
	if (requests === 1)
		$('#loading').show();
}).bind('ajaxComplete', function() {
	requests--;
	if (requests === 0)
		$('#loading').fadeOut();
});