/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-07-27
// Description:
// JavaScript functions and handling for user management
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/

$(function() {
	$('#userTabs').tabs();
	
	$('#newUserCreate').click(function() {
		var user = {
			firstname: $('#newUserFirstname').val().trim(),
			lastname: $('#newUserLastname').val().trim(),
			email: $('#newUserEmail').val().trim(),
			admin: $('#newUserAdmin').is(':checked')
		};
		
		api('createUser', user, function(response) {
			if (response.error === '') {
				if (response.data === true) {
					$('#newUserSaveSuccess').show();
					setTimeout(function() {
						$('#newUserSaveSuccess').fadeOut();
					}, 5000);
					
					$('#newUserFirstname').val('');
					$('#newUserLastname').val('');
					$('#newUserEmail').val('');
					$('#newUserAdmin').prop("checked", false);
				} else {
					alert(response.data);
				}
			} else {
				console.log(response.error);
				alert(response.error);
			}
		});
		
		return false;
	});
	
	$('#editUserTab').click(function() {
		api('loadEditUserSelect', null, function(response) {
			if (response.error === '') {
				$('#editUserSelect').children('option:not(:first)').remove();
				
				$.each(response.data, function(idx, row) {
					$('#editUserSelect').append(row);
				});
			} else {
				console.log(response.error);
			}
		}, false);
	});
	
	$('#editUserEdit').click(function() {
		var id = $('#editUserSelect').val();
		if (id === '')
			return;
		
		api('dialogUserSettings', id, function(response) {
			if (response.error === '') {
				$('body').append(response.data);
				$('#dialogUserSettings').dialog({
					width: 500,
					modal: true,
					close: function() {
						$(this).remove();
					}
				});
			} else {
				console.log(response.error);
			}
		});
	});
	
	$('#editUserDelete').click(function() {
		var id = $('#editUserSelect').val();
		if (id === '')
			return;
		
		var question = '';
		api('tx', 'WebCal.Dialogs.EditUser.ConfrimDelete', function(response) {
			if (response.error === '') {
				question = response.data;
			} else {
				console.log(response.error);
			}
		}, false);
		
		if (confirm(question)) {
			api('deleteUser', id, function(response) {
				if (response.error === '') {
					$('#editUserSelect option:selected').remove();
				} else {
					console.log(response.error);
					alert(response.error);
				}
			});
		}
	});
});