/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-06-29
// Description:
// JavaScript functions and handling for calendar view
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/

// load all parts of the calendar asynchrounous and display them
// @param view: if set, we'll try to work with that view
function loadCalendar(part) {
	if (part === undefined || part == 'head') {
		api('calHead', null, function(response) {
			if (response.error === '') {
				$('#calMenuTitle').text(response.data);
			} else {
				$('#calMenuTitle').text(response.error);
			}
		}, true, true);
	}
	
	if (part === undefined || part == 'grid') {
		api('calGrid', null, function(response) {
			if (response.error === '') {
				$('#calGrid').html(response.data);
			} else {
				$('#calGrid').html("<strong>Error:</strong> " + response.error);
			}
		}, true, true);
	}
	
	if (part === undefined || part == 'dates') {
		api('calDates', null, function(response) {
			if (response.error === '') {
				$('#calDates').html(response.data.html);
				$('#calDates').css({
					width: response.data.width,
					height: response.data.height
				});
			} else {
				$('#calDates').text(response.error);
			}
		}, true, true);
	}
}

// a try to rebuild PHPs date()
// @param format: format of date or datetime or time
// @param date: if set, that date will be used, eles current date
function dateFormat(format, date) {
	var dayShort = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
	var dayLong = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

	if (date === undefined || date === null)
		date = new Date();

	var j = date.getDate();
	var d = (j < 10) ? '0' + j : j;

	var n = date.getMonth() + 1;
	var m = (n < 10) ? '0' + n : n;

	var Y = date.getFullYear();

	var G = date.getHours();
	var H = (G < 10) ? '0' + G : G;
	
	var g = (G % 12) + 1;
	var h = (g < 10) ? '0' + g : g;

	var i = date.getMinutes();
	if (i < 10) i = '0' + i;

	var s = date.getSeconds();
	if (s < 10) s = '0' + s;

	var w = date.getDay();
	var N = w+1;

	var out = format;
	out = out.replace('j', j);
	out = out.replace('d', d);
	out = out.replace('n', n);
	out = out.replace('m', m);
	out = out.replace('Y', Y);
	out = out.replace('G', G);
	out = out.replace('H', H);
	out = out.replace('g', g);
	out = out.replace('h', h);
	out = out.replace('i', i);
	out = out.replace('s', s);
	
	out = out.replace('w', w);
	out = out.replace('N', N);
	
	out = out.replace('D', dayShort[w]);
	
	return out;
}

// open up a dialog with details of date and options to edit or delete
// @param obj: clicked element as object
function dialogDateAction(obj) {
	var date = {};
	api('calGetDate', $(obj).attr('dateid'), function(response) {
		if (response.error === '') {
			date = response.data;
		} else {
			console.log(response.error);
		}
	}, false);
	
	if (date.dateid === undefined)
		return;
	
	api('dialogDateAction', date, function(response) {
		if (response.error === '') {
			$('body').append(response.data);
			$('#dialogDateAction').dialog({
				width: 400,
				modal: true,
				close: function() {
					$(this).remove();
				}
			});
		} else {
			console.log(response.error);
		}
	});
}

// all other jQuery handling
$(function() {
	loadCalendar();
	
	// switch views
	$('#viewCalDay').click(function() {
		$_SESSION('View', 'day');
		loadCalendar();
		return false;
	});
	$('#viewCalWeek').click(function() {
		$_SESSION('View', 'week');
		loadCalendar();
		return false;
	});
	$('#viewCalMonth').click(function() {
		$_SESSION('View', 'month');
		loadCalendar();
		return false;
	});
	$('#viewCalToday').click(function() {
		$_SESSION('day', dateFormat('Y-m-d'));
		loadCalendar();
		$('#sideCal').datepicker('setDate', new Date());
		return false;
	});
	$('#viewCalGoto').click(function() {
		api('dialogGoto', {}, function(response) {
			if (response.error === '') {
				$('body').append(response.data);
				$('#dialogGoto').dialog({
					width: 400,
					modal: true,
					close: function() {
						$(this).remove();
					}
				});
				$('#gotoInput').change(function() {
					var date = $(this).val().split('.');
					if (date.length == 3 && date[0].length == 2 && date[1].length == 2 && date[2].length == 4
							&& 0 < date[0] && date[0] < 32 && 0 < date[1] && date[1] < 13 && 0 <= date[2]) {
						var day = date[2] + '-' + date[1] + '-' + date[0];
						$_SESSION('day', day);
						loadCalendar();
						$('#sideCal').datepicker('setDate', new Date(day));
						$('#dialogGoto').dialog('close').remove();
					}
				});
			} else {
				console.log(response.error);
			}
		});
		return false;
	});
	
	// arrow buttons to get previous or next period
	$('#calMenuPrev').click(function() {
		var day = new Date($_SESSION('day'));
		switch ($_SESSION('View')) {
			case 'day':
				day.setDate(day.getDate() - 1);
				break;
			case 'week':
				day.setDate(day.getDate() - 7);
				break;
			case 'month':
				day.setMonth(day.getMonth() - 1);
				break;
			default:
				console.log("Unknown View found. Do nothing.");
				return false;
		}
		$_SESSION('day', dateFormat('Y-m-d', day));
		loadCalendar();
		$('#sideCal').datepicker('setDate', day);
	});
	$('#calMenuNext').click(function() {
		var day = new Date($_SESSION('day'));
		switch ($_SESSION('View')) {
			case 'day':
				day.setDate(day.getDate() + 1);
				break;
			case 'week':
				day.setDate(day.getDate() + 7);
				break;
			case 'month':
				day.setMonth(day.getMonth() + 1);
				break;
			default:
				console.log("Unknown View found. Do nothing.");
				return false;
		}
		$_SESSION('day', dateFormat('Y-m-d', day));
		loadCalendar();
		$('#sideCal').datepicker('setDate', day);
	});
	// arrow keys simulating press on buttons above
	$(document).keydown(function(key) {
		if (!$('#calDates').is(':visible')
			|| $('.ui-widget-overlay').is(':visible'))
			return;
		
		switch (key.which) {
			case 37: // left arrow
				$('#calMenuPrev').click();
				break;
			case 39: // right arrow
				$('#calMenuNext').click();
				break;
			default:
				return;
		}
	});
	
	$(document).on('click', '#calDates .dateList li', function() {
		dialogDateAction(this);
	}).on('click', '.date', function() {
		dialogDateAction(this);
	});
	
	$('#createNewDate').click(function() {
		api('dialogNewDate', null, function(response) {
			if (response.error === '') {
				$('body').append(response.data);
				$('.datetimepicker').datetimepicker({
					dateFormat: 'dd.mm.yy',
					timeFormat: 'HH:mm'
				});
				$('#dialogNewDate').dialog({
					width: 400,
					modal: true,
					close: function() {
						$(this).remove();
					}
				});
			} else {
				console.log(response.error);
			}
		});
		return false;
	});
});
