-- -----------------------------------------------------------------------------
-- Author:   Andreas Mueller <webmaster@am-wd.de>
-- Created:  2015-07-30
-- Description:
-- Database Structure / Schema for SQLite Databases
-- -----------------------------------------------------------------------------

-- delete tables in order of foreign keys
DROP TABLE IF EXISTS {PFX}dates;
DROP TABLE IF EXISTS {PFX}settings;
DROP TABLE IF EXISTS {PFX}users;

--
-- create table for users
--
CREATE TABLE {PFX}users (
    userid    INTEGER      NOT NULL PRIMARY KEY AUTOINCREMENT
  , firstname TEXT(50,0)
  , lastname  TEXT(50,0)
  , email     TEXT(100,0)  NOT NULL
  , pass      TEXT(255,0)  NOT NULL
  , bgcolor   TEXT(10,0)   NOT NULL DEFAULT 'ffffff'
  , fcolor    TEXT(10,0)   NOT NULL DEFAULT '333333'
  , admin     INTEGER(1,0) NOT NULL DEFAULT 0
  , notify    INTEGER(4,0) NOT NULL DEFAULT 3
  , cal       TEXT(10,0)   NOT NULL DEFAULT 'public'
  , daybegin  TEXT(20,0)   NOT NULL
  , dayend    TEXT(20,0)   NOT NULL
  , view      TEXT(10,0)   NOT NULL DEFAULT 'week'
  , lastLogin TEXT(20,0)
  , CONSTRAINT email_uq UNIQUE (email ASC)
);

--
-- create table for settings
--
CREATE TABLE {PFX}settings (
    settingsid      INTEGER       NOT NULL PRIMARY KEY AUTOINCREMENT
  , title           TEXT(40,0)
  , slogan          TEXT(80,0)
  , company         TEXT(50,0)
  , companyLink     TEXT(150,0)
  , daybegin        TEXT(20,0)    NOT NULL
  , dayend          TEXT(20,0)    NOT NULL
  , defaultView     TEXT(10,0)    NOT NULL DEFAULT 'week'
  , previewLimit    INTEGER(3,0)  NOT NULL DEFAULT 3
  , attachement     INTEGER(1,0)  NOT NULL DEFAULT 0
  , attachementSize INTEGER(10,0) NOT NULL DEFAULT 2097152
  , publicAccess    INTEGER(1,0)  NOT NULL DEFAULT 0
  , loading         INTEGER(1,0)  NOT NULL DEFAULT 1
  , showClock       INTEGER(1,0)  NOT NULL DEFAULT 1
);

--
-- create table for appointments
--
CREATE TABLE {PFX}dates (
    dateid   INTEGER     NOT NULL PRIMARY KEY AUTOINCREMENT
  , title    TEXT(100,0) NOT NULL
  , location TEXT(100,0)
  , guest    TEXT(100,0)
  , "begin"  TEXT(20,0)  NOT NULL
  , "end"    TEXT(20,0)  NOT NULL
  , created  TEXT(20,0)  NOT NULL
  , detail   TEXT
  , public   INTEGER(1,0)
  , userid   INTEGER(10,0)
  , CONSTRAINT userid_fk FOREIGN KEY (userid) REFERENCES {PFX}users(userid) ON DELETE CASCADE ON UPDATE CASCADE
);