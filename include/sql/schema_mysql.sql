-- -----------------------------------------------------------------------------
-- Author:   Andreas Mueller <webmaster@am-wd.de>
-- Created:  2015-07-30
-- Description:
-- Database Structure / Schema for MySQL Databases
-- -----------------------------------------------------------------------------

-- delete tables in order of foreign keys
DROP TABLE IF EXISTS {PFX}dates;
DROP TABLE IF EXISTS {PFX}settings;
DROP TABLE IF EXISTS {PFX}users;

--
-- create table for users
--
CREATE TABLE {PFX}users (
    userid    INT(10)      NOT NULL AUTO_INCREMENT
  , firstname VARCHAR(50)
  , lastname  VARCHAR(50)
  , email     VARCHAR(100) NOT NULL
  , pass      VARCHAR(255) NOT NULL
  , bgcolor   VARCHAR(10)  NOT NULL DEFAULT 'ffffff'
  , fcolor    VARCHAR(10)  NOT NULL DEFAULT '333333'
  , admin     BIT(1)       NOT NULL DEFAULT 0
  , notify    INT(4)       NOT NULL DEFAULT 3
  , cal       VARCHAR(10)  NOT NULL DEFAULT 'public'
  , daybegin  DATETIME     NOT NULL
  , dayend    DATETIME     NOT NULL
  , view      VARCHAR(10)  NOT NULL DEFAULT 'week'
  , lastLogin DATETIME
  , PRIMARY KEY (userid)
  , UNIQUE KEY email_qu (email)
);

--
-- create table for settings
--
CREATE TABLE {PFX}settings (
    settingsid      INT(10)      NOT NULL AUTO_INCREMENT
  , title           VARCHAR(40)
  , slogan          VARCHAR(80)
  , company         VARCHAR(50)
  , companyLink     VARCHAR(150)
  , daybegin        DATETIME     NOT NULL
  , dayend          DATETIME     NOT NULL
  , defaultView     VARCHAR(10)  NOT NULL DEFAULT 'week'
  , previewLimit    INT(3)       NOT NULL DEFAULT 3
  , attachement     BIT(1)       NOT NULL DEFAULT 0
  , attachementSize INT(10)      NOT NULL DEFAULT 2097152
  , publicAccess    BIT(1)       NOT NULL DEFAULT 0
  , loading         BIT(1)       NOT NULL DEFAULT 1
  , showClock       BIT(1)       NOT NULL DEFAULT 1
  , PRIMARY KEY (settingsid)
);

--
-- create table for appointments
--
CREATE TABLE {PFX}dates (
    dateid   INT(10)      NOT NULL AUTO_INCREMENT
  , title    VARCHAR(100) NOT NULL
  , location VARCHAR(100)
  , guest    VARCHAR(100)
  , `begin`  DATETIME     NOT NULL
  , `end`    DATETIME     NOT NULL
  , created  DATETIME     NOT NULL
  , detail   TEXT
  , public   BIT(1)
  , userid   INT(10)
  , PRIMARY KEY (dateid)
  , KEY userid_k(userid)
  , CONSTRAINT userid_fk FOREIGN KEY (userid) REFERENCES {PFX}users(userid) ON DELETE CASCADE ON UPDATE CASCADE
);