<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-23
// Description:
// Login handling
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
use AMWD\Tx as Tx;

if (isset($_GET['p']) && $_GET['p'] == 'login') {
	$get = $_SERVER['QUERY_STRING'];
	
	$layout['navLeft'] = Tx::T('WebCal.Menu.State.LoggedOut');
	$layout['navRight'] = '<a href="'.URL.'admin.php?p=passwd">'.Tx::T('WebCal.Menu.Action.LostPassword').'</a>';
	$layout['content'] = '<h1 class="center">'.Tx::T('WebCal.Sites.Login.Title').'</h1>';
	
	if (isset($_POST['action']) && $_POST['action'] == Tx::T('WebCal.Sites.Login.Action.Login')
			&& !empty($_POST['email']) && !empty($_POST['password'])) {
		
		$sql->open();
		$res = $sql->query("SELECT * FROM ".$config['pfx']."users WHERE email = '".$_POST['email']."'");
		
		if ($sql->num_rows($res) == 1) {
			$obj = $sql->fetch_object($res);
			$sql->close();
			
			// successful logged in
			if ($obj->pass == crypt($_POST['password'], $obj->pass)) {
				$_SESSION['LoginState'] = true;
				$_SESSION['PreviewLimit'] = $obj->notify;
				$_SESSION['Calendar'] = $obj->cal;
				$_SESSION['Admin'] = ($obj->admin == 1);
				$_SESSION['uID'] = $obj->userid;
				
				$sql->open();
				$sql->query("UPDATE ".$config['pfx']."users SET lastLogin = ".date('Y-m-d H:i:s')." WHERE userid = ".$obj->userid);
				$sql->close();
				
				header('Location: '.URL.'index.php');
			} else {
				$layout['content'] .= jqAlert(Tx::T('WebCal.Sites.Login.Label.LoginFailed'), 'alert', 'error');
			}
		} else {
			$sql->close();
			$layout['content'] .= jqAlert(Tx::T('WebCal.Sites.Login.Label.LoginFailed'), 'alert', 'error');
		}
	}
	
	$layout['content'] .= '
	<form action="'.URL.'admin.php'.(empty($get) ? '' : '?'.$get).'" method="post">
	<table>
		<tr>
			<td>'.Tx::T('WebCal.Sites.Login.Label.Email').':</td>
			<td><input type="email" name="email" placeholder="test@example.com" /></td>
		</tr>
		<tr>
			<td>'.Tx::T('WebCal.Sites.Login.Label.Password').':</td>
			<td><input type="password" name="password" placeholder="Password" /></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" class="button" name="action" value="'.Tx::T('WebCal.Sites.Login.Action.Login').'" /></td>
		</tr>
	</table>
	</form>
	';
} else if ($_GET['p'] == 'passwd') {
	$get = $_SERVER['QUERY_STRING'];
	
	$layout['navLeft'] = Tx::T('WebCal.Menu.State.LoggedOut');
	$layout['navRight'] = '<a href="'.URL.'admin.php?p=login">'.Tx::T('WebCal.Menu.Action.Login').'</a>';
	$layout['content'] = '<h1 class="center">'.Tx::T('WebCal.Sites.LostPassword.Title').'</h1>';
	
	if (isset($_POST['action']) && $_POST['action'] == Tx::T('WebCal.Sites.LostPassword.Action.SendEmail') && !empty($_POST['email'])) {
		$salt = randomString(20); $pass = randomString(8);
		
		$sql->open();
		$res = $sql->query("UPDATE ".$config['pfx']."users SET pass = '".crypt($pass, '$2a$07$'.$salt.'$')."' WHERE email = '".$_POST['email']."'");
		if ($sql->affected_rows() == 1) {
			$msg = Tx::T('WebCal.Sites.LostPassword.Email.Text', array('PASSWORD' => $pass));
			
			if (mail($_POST['email'], Tx::T('WebCal.Sites.LostPassword.Email.Subject'), $msg, "From: no-reply@".$_SERVER['HTTP_HOST'], "-fno-reply@".$_SERVER['HTTP_HOST'])) {
				$layout['content'] .= jqAlert(Tx::T('WebCal.Sites.LostPassword.Label.EmailSent'), 'check');
				$layout['content'] .= '<div class="center"><a href="'.URL.'admin.php'.(empty($get) ? '' : '?'.str_replace('p=passwd', 'p=login', $get)).'" class="button">'.Tx::T('WebCal.Sites.LostPassword.Action.Next').'</a></div>';
			} else {
				$layout['content'] .= jqAlert(Tx::T('WebCal.Sites.LostPassword.Error.EmailNotSent'), 'closethick', 'error');
				
				$layout['content'] .= nl2br($msg);

				$layout['content'] .= '<div class="center"><a href="'.URL.'admin.php'.(empty($get) ? '' : '?'.str_replace('p=passwd', 'p=login', $get)).'" class="button">'.Tx::T('WebCal.Sites.LostPassword.Action.Next').'</a></div>';
			}
			
			
		} else {
			$layout['content'] .= jqAlert(Tx::T('WebCal.Sites.LostPassword.Error.EmailAddress'), 'closethick', 'error').'
			<div class="center"><a href="'.URL.'admin.php'.(empty($get) ? '' : '?'.$get).'" class="button">'.Tx::T('WebCal.Sites.LostPassword.Action.Back').'</a></div>
			';
		}
		$sql->close();
	} else {
		$layout['content'] .= '
		<form action="'.URL.'admin.php'.(empty($get) ? '' : '?'.$get).'" method="post">
		<table>
			<tr>
				<td>'.Tx::T('WebCal.Sites.LostPassword.Label.Email').':</td>
				<td><input type="email" name="email" placeholder="test@example.com" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" class="button" name="action" value="'.Tx::T('WebCal.Sites.LostPassword.Action.SendEmail').'" /></td>
			</tr>
		</table>
		</form>
		';
	}
} else {
	session_destroy();
	
	header('Location: '.URL);
	exit;
}

?>