<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-07-16
// Description:
// general settings for calendar
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
defined('admin') || die('<strong>Error:</strong> no admin area');
use AMWD\Tx as Tx;

$layout['javascript'][] = URL.'include/js/settings.js';

$query = "
SELECT
	*
FROM
	".$config['pfx']."settings
WHERE
	settingsid = 1";

$sql->open();
$res = $sql->query($query);
$row = $sql->fetch_object($res);
$sql->close();

$selectedDay = $selectedWeek = $selectedMonth = '';
switch ($row->defaultView) {
	case 'day': $selectedDay = ' selected="selected"'; break;
	case 'week': $selectedWeek = ' selected="selected"'; break;
	case 'month': $selectedMonth = ' selected="selected"'; break;
}

$checkedAttachement = ($row->attachement == 1) ? 'checked="checked" ' : '';
$checkedPublicAccess = ($row->publicAccess == 1) ? 'checked="checked" ' : '';
$checkedLoading = ($row->loading == 1) ? 'checked="checked" ' : '';
$checkedClock = ($row->showClock == 1) ? 'checked="checked" ' : '';

$layout['content'] = '
<h1>'.Tx::T('WebCal.Sites.Admin.Settings.Heading').'</h1>

<span id="settingsSaveSuccess">
	'.jqAlert(Tx::T('WebCal.Sites.Admin.Settings.SaveSuccess'), 'circle-check').'
</span>

<table>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Title.Label').'</td>
		<td><input type="text" id="settingsTitle" size="30" value="'.$row->title.'" /></td>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Title.Description').'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Subtitle.Label').'</td>
		<td><input type="text" id="settingsSubtitle" size="30" value="'.$row->slogan.'" /></td>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Subtitle.Description').'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Company.Label').'</td>
		<td><input type="text" id="settingsCompany" size="30" value="'.$row->company.'" /></td>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Company.Description').'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.CompanyLink.Label').'</td>
		<td><input type="text" id="settingsCompanyLink" size="30" value="'.$row->companyLink.'" /></td>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.CompanyLink.Description').'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Daybegin.Label').'</td>
		<td><input type="text" id="settingsDaybegin" size="30" value="'.date('H:i', strtotime($row->daybegin)).'" /></td>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Daybegin.Description').'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Dayend.Label').'</td>
		<td><input type="text" id="settingsDayend" size="30" value="'.date('H:i', strtotime($row->dayend)).'" /></td>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Dayend.Description').'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.View.Label').'</td>
		<td>
			<select id="settingsView">
				<option value="day"'.$selectedDay.'>'.Tx::T('WebCal.Menu.View.Day').'</option>
				<option value="week"'.$selectedWeek.'>'.Tx::T('WebCal.Menu.View.Week').'</option>
				<option value="month"'.$selectedMonth.'>'.Tx::T('WebCal.Menu.View.Month').'</option>
			</select>
		</td>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.View.Description').'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.PreviewLimit.Label').'</td>
		<td><input type="number" id="settingsPreviewLimit" size="30" value="'.$row->previewLimit.'" /></td>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.PreviewLimit.Description').'</td>
	</tr>
	<!--tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Attachement.Label').'</td>
		<td><label><input type="checkbox" id="settingsAttachement" '.$checkedAttachement.'/> '.Tx::T('WebCal.Sites.Admin.Settings.Attachement.Description').'</label></td>
		<td></td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.AttachementSize.Label').'</td>
		<td><input type="number" id="settingsAttachementSize" size="30" value="'.$row->attachementSize.'" /> KB</td>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.AttachementSize.Description').'</td>
	</tr-->
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.PublicAccess.Label').'</td>
		<td><label><input type="checkbox" id="settingsPublicAccess" '.$checkedPublicAccess.'/> '.Tx::T('WebCal.Sites.Admin.Settings.PublicAccess.Description').'</label></td>
		<td></td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Loading.Label').'</td>
		<td><label><input type="checkbox" id="settingsLoading" '.$checkedLoading.'/> '.Tx::T('WebCal.Sites.Admin.Settings.Loading.Description').'</label></td>
		<td></td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Settings.Clock.Label').'</td>
		<td><label><input type="checkbox" id="settingsClock" '.$checkedClock.'/> '.Tx::T('WebCal.Sites.Admin.Settings.Clock.Description').'</label></td>
		<td></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td><button id="settingsSave">'.Tx::T('WebCal.Sites.Admin.Settings.Save').'</button></td>
		<td></td>
	</tr>
</table>
';

?>