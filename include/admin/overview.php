<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-07-16
// Description:
// overview
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
defined('admin') || die('<strong>Error:</strong> no admin area');
use AMWD\Tx as Tx;

$sql->open();
$res = $sql->query("SELECT COUNT(*) as count FROM ".$config['pfx']."users");
$numberUsers = $sql->fetch_object($res)->count;

$res = $sql->query("SELECT COUNT(*) as count FROM ".$config['pfx']."users WHERE admin = 1");
$numberAdmins = $sql->fetch_object($res)->count;

$res = $sql->query("SELECT COUNT(*) as count FROM ".$config['pfx']."dates WHERE public = 1");
$numberPublicDates = $sql->fetch_object($res)->count;

$res = $sql->query("SELECT COUNT(*) as count FROM ".$config['pfx']."dates WHERE public = 0");
$numberPrivateDates = $sql->fetch_object($res)->count;

$dbversion = $sql->getVersion();
$sql->close();

$layout['content'] = '
<h1>'.Tx::T('WebCal.Sites.Admin.Overview.Heading').'</h1>
<h3>'.Tx::T('WebCal.Sites.Admin.Overview.StatisticHeading').'</h3>
<table>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Overview.CountUsers').'</td>
		<td>'.$numberUsers.'</td>
		<td>'.Tx::T('WebCal.Sites.Admin.Overview.CountPublicDates').'</td>
		<td>'.$numberPublicDates.'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Overview.CountAdmins').'</td>
		<td>'.$numberAdmins.'</td>
		<td>'.Tx::T('WebCal.Sites.Admin.Overview.CountPrivateDates').'</td>
		<td>'.$numberPrivateDates.'</td>
	</tr>
</table>

<h3>'.Tx::T('WebCal.Sites.Admin.Overview.TechnicHeading').'</h3>
<table>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Overview.DatabaseType').'</td>
		<td>'.$config['type'].'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Overview.DatabaseVersion').'</td>
		<td>'.$dbversion.'</td>
	</tr>
	<tr>
		<td>'.Tx::T('WebCal.Sites.Admin.Overview.PHPVersion').'</td>
		<td>'.phpversion().'</td>
	</tr>
</table>
';

?>