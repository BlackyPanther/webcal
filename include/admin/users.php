<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-07-27
// Description:
// manage users
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
defined('admin') || die('<strong>Error:</strong> no admin area');
use AMWD\Tx as Tx;

$layout['javascript'][] = URL.'include/js/users.js';

$query = "
SELECT
	userid,
	firstname,
	lastname
FROM
	".$config['pfx']."users
ORDER BY
	lastname, firstname, userid";

$sql->open();
$res = $sql->query($query);

$userList = array();
while ($row = $sql->fetch_object($res)) {
	$userList[] = '<option value="'.$row->userid.'">'.$row->lastname.', '.$row->firstname.'</option>';
}

$sql->close();

$layout['content'] = '
<h1>'.Tx::T('WebCal.Sites.Admin.Users.Heading').'</h1>

<div id="userTabs">
	<ul>
		<li><a href="#editUser" id="editUserTab">'.Tx::T('WebCal.Sites.Admin.Users.ExistingUsers').'</a></li>
		<li><a href="#newUser">'.Tx::T('WebCal.Sites.Admin.Users.NewUser').'</a></li>
	</ul>
	
	<div id="newUser">
		<span id="newUserSaveSuccess">
			'.jqAlert(Tx::T('WebCal.Sites.Admin.Users.SaveSuccess'), 'circle-check').'
		</span>

		<table>
			<tr>
				<td>'.Tx::T('WebCal.Sites.Admin.Users.Firstname').'</td>
				<td><input type="text" id="newUserFirstname" size="30" /></td>
				<td>'.Tx::T('WebCal.Sites.Admin.Users.Lastname').'</td>
				<td><input type="text" id="newUserLastname" size="30" /></td>
			</tr>
			<tr>
				<td>'.Tx::T('WebCal.Sites.Admin.Users.Email').'</td>
				<td><input type="text" id="newUserEmail" size="30" /></td>
				<td>'.Tx::T('WebCal.Sites.Admin.Users.Admin').'</td>
				<td><label><input type="checkbox" id="newUserAdmin" /> '.Tx::T('WebCal.Sites.Admin.Users.AdminRights').'</label></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><button id="newUserCreate">'.Tx::T('WebCal.Sites.Admin.Users.Create').'</button></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
	<div id="editUser" class="center">
		'.Tx::T('WebCal.Sites.Admin.Users.SelectUser').':
		<select id="editUserSelect">
			<option value="">'.Tx::T('WebCal.Sites.Admin.Users.Select').'</option>
			'.implode(PHP_EOL, $userList).'
		</select>
		<br />
		<br />
		<button id="editUserEdit">'.Tx::T('WebCal.Sites.Admin.Users.Edit').'</button>
		<button id="editUserDelete">'.Tx::T('WebCal.Sites.Admin.Users.Delete').'</button>
	</div>
</div>
';

?>