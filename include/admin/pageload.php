<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-23
// Description:
// Check which page to load
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
use AMWD\Tx as Tx;

if (isset($_GET['p']) && ($_GET['p'] == 'login' || $_GET['p'] == 'lostpw' || $_GET['p'] == 'logout')) {
	require_once DIR.'include/admin/log.php';
} else if (isset($_SESSION['LoginState']) && isset($_SESSION['Admin'])
		&& $_SESSION['LoginState'] && $_SESSION['Admin'] == 1) {
	$sql->open();
	$res = $sql->query("SELECT lastLogin, firstname, lastname FROM ".$config['pfx']."users WHERE userid = ".$_SESSION['uID']);
	$row = $sql->fetch_object($res);
	$sql->close();
	$row->name = $row->firstname.' '.$row->lastname;

	$layout['navLeft'] = Tx::T('WebCal.Menu.State.LoggedIn').': <a href="#" class="showUserSettings" title="'.Tx::T('WebCal.Menu.State.LastLogin').': '.date('d.m.Y H:i', strtotime($row->lastLogin)).'">'.$row->name.'</a><br />';
	$layout['navRight'] = '<a href="'.URL.'admin.php?p=logout">'.Tx::T('WebCal.Menu.Action.Logout').'</a><br />';
	
	$layout['side'] = '
	<ul id="menu">
		<li><a href="'.URL.'admin.php"><span class="ui-icon ui-icon-arrowreturnthick-1-n"></span> '.Tx::T('WebCal.Sites.Admin.Overview.Heading').'</a></li>
		<li><a href="'.URL.'admin.php?part=settings"><span class="ui-icon ui-icon-wrench"></span> '.Tx::T('WebCal.Sites.Admin.Settings.Heading').'</a></li>
		<li><a href="'.URL.'admin.php?part=user"><span class="ui-icon ui-icon-person"></span> '.Tx::T('WebCal.Sites.Admin.Users.Heading').'</a></li>
		<li><a href="'.URL.'index.php"><span class="ui-icon ui-icon-arrowreturnthick-1-w"></span> '.Tx::T('WebCal.Sites.Admin.BackToCalendar').'</a></li>
	</ul>
	';
	
	
	switch ($_GET['part']) {
		case 'settings':
			include DIR.'include/admin/settings.php';
			break;
		case 'user':
			include DIR.'include/admin/users.php';
			break;
		default:
			include DIR.'include/admin/overview.php';
			break;
	}
} else if (isset($_SESSION['LoginState']) && isset($_SESSION['Admin'])
		&& $_SESSION['LoginState'] && $_SESSION['Admin'] != 1) {
	header('Location: '.URL.'index.php');
	exit;
} else {
	require_once DIR.'include/admin/log.php';
}
?>