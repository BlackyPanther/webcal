<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-13
// Description:
// Head-File - Load all files needed
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
// 2015-03-23 | Added switch for admin page
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
defined('DIR') || die('<strong>Error:</strong> Base path is not set properly');

if (file_exists(DIR.'installation.php') && !file_exists(DIR.'include/config.php'))
	header('Location: installation.php');
if (file_exists(DIR.'installation.php') && file_exists(DIR.'include/config.php'))
	@unlink(DIR.'installation.php');

require_once DIR.'include/config.php';
require_once DIR.'include/submodule/sql/sql.class.php';
use AMWD\SQL as SQL;

if ($config['type'] == 'sqlite') {
	$sql = SQL::SQLite($config['path']);
} else {
	$sql = SQL::MySQL($config['user'], $config['pass'], $config['base'], $config['port'], $config['host']);
}

$GLOBALS['sql'] = $sql;

require_once DIR.'include/php/session.php';
require_once DIR.'include/php/functions.php';

if (!empty($_GET['lang']))
	$_SESSION['lang'] = $_GET['lang'];

// first existence of this class - work in progress
require_once DIR.'include/php/Tx.class.php';
use AMWD\Tx as Tx;

Tx::LoadDirectory(DIR.'/include/lang/');
Tx::SetLanguage($_SESSION['lang']);

$layout['title'] = Tx::T('WebCal.Title');
$layout['slogan'] = Tx::T('WebCal.Slogan');
$layout['headStyle'] =
$layout['navLeft'] =
$layout['navRight'] =
$layout['side'] =
$layout['content'] =
$layout['company'] =
$layout['companyLink'] = '';
$layout['javascript'] = array();

?>