<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-05-28
// Description:
// load session and define defaults if needed
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');

session_name('WebCal'.md5(date('Y')));
session_start();

$sql->open();
$res = $sql->query("SELECT publicAccess, previewLimit, defaultView FROM ".$config['pfx']."settings WHERE settingsid = 1");

if ($sql->num_rows($res) == 1) {
	$row = $sql->fetch_object($res);
	$_SESSION['PublicAccess'] = ($row->publicAccess == 1);
	$_SESSION['PreviewLimit'] = $row->previewLimit;
	if (empty($_SESSION['View']))
		$_SESSION['View'] = $row->defaultView;
}
$sql->close();

if (empty($_SESSION['lang']))
	$_SESSION['lang'] = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

?>