<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-13
// Description:
// HTML Layout for Page
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
use AMWD\Tx as Tx;

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=1000, initial-scale=1.0" />
		
		<title>..:: <?=$layout['title']?> ::..</title>
		
		<link rel="stylesheet" type="text/css" href="<?=URL.'include/css/'?>jqueryui.css" />
		<link rel="stylesheet" type="text/css" href="<?=URL.'include/submodule/datetimepicker/dist/'?>jquery-ui-timepicker-addon.min.css" />
		<link rel="stylesheet" type="text/css" href="<?=URL.'include/submodule/colorpicker/'?>jquery.colorpicker.css" />
		<link rel="stylesheet" type="text/css" href="<?=URL.'include/fonts/'?>fonts.css" />
		<link rel="stylesheet" type="text/css" href="<?=URL.'include/css/'?>main.css" />
		<link rel="stylesheet" type="text/css" href="<?=URL.'include/css/'?>jqueryui-fixes.css" />
		
		<style type="text/css">
			<?=$layout['headStyle']?>
		</style>
		
		<script type="text/javascript" src="<?=URL.'include/js/'?>jquery.js"></script>
		<script type="text/javascript" src="<?=URL.'include/js/'?>jqueryui.js"></script>
		<script type="text/javascript" src="<?=URL.'include/submodule/datetimepicker/dist/'?>jquery-ui-timepicker-addon.min.js"></script>
		<script type="text/javascript" src="<?=URL.'include/submodule/colorpicker/'?>jquery.colorpicker.js"></script>
		<script type="text/javascript" src="<?=URL.'include/js/'?>main.js"></script>
		<?php
		foreach ($layout['javascript'] as $js) {
			?>
		<script type="text/javascript" src="<?=$js?>"></script>
			<?php
		}
		?>
	</head>
	
	<body>
		<div id="loading">
			<div id="loadingAnimation"></div>
		</div>
		<div id="javascriptAlert">
			<div id="javascriptAlertText">
				<p>
					<?=nl2br(Tx::T('WebCal.JSError'))?>
				</p>
			</div>
		</div>
		<div id="wrapper">
			
			<div id="headWrapper">
				<div id="headMain">
					<a href="<?=URL?>"><h1><?=$layout['title']?></h1></a>
					<h3><?=$layout['slogan']?></h3>
				</div>
				
				<div id="headClock">
					<span id="clkTime"><?=date('H:i')?></span>
					<br />
					<span id="clkDate"><?=date('d.m.Y')?></span>
				</div>
			</div>
			
			<div id="navWrapper" class="blue">
				<div id="navLeft">
					<?=$layout['navLeft']?>
				</div>
				
				<div id="navRight">
					<?=$layout['navRight']?>
				</div>
				
				<br class="clear" />
			</div>
			
			<div id="mainWrapper">
				<div id="mainLeft">
					<?=$layout['side']?>
				</div>
				
				<div id="mainRight">
					<?=$layout['content']?>
				</div>
				
				<br class="clear" />
			</div>
			
			<div id="footWrapper" class="blue">
				<div id="footLeft">&copy; <?=date('Y')?> | <a href="<?=$layout['companyLink']?>" class="blank"><?=$layout['company']?></a></div>
				<div id="footRight"><a href="http://am-wd.de" class="blank">WebCal <?=$config['version']?></a></div>
				<br class="clear" />
			</div>
			
		</div>
	</body>
</html>