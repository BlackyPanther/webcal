<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-13
// Description:
// Receives a base64 encoded json-String via GET and creates ics File
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/

/* Testeintrag: 31.03.2015 11:00 - 12:00 Uhr
eyJiZWdpbiI6MTQyNzc5MjQwMCwiZW5kIjoxNDI3Nzk2MDAwLCJzdW1tYXJ5IjoiVGVzdCBFaW50cmFnIiwiZGVzY3JpcHRpb24iOiJXdW5kZXJ2b2xsZXIgVGVzdGVpbnRhZyBpbSBpY3MtRm9ybWF0IiwibG9jYXRpb24iOiJCYWhuaG9mc3RyYVx1MDBkZmUgMSwgOTEwNTIgRXJsYW5nZW4iLCJ1cmwiOiJodHRwOlwvXC9hbS13ZC5kZSJ9
*/

if (!isset($_GET['data']) || empty($_GET['data'])) die("<strong>Error:</strong> no data");

$data = json_decode(base64_decode($_GET['data']));

if (empty($data)) die("<strong>Error:</strong> data string invalid");

function escapeSequence($sequence) {
	return preg_replace('/([\,;])/','\\\$1', $sequence);
}

function dateTime($unix) {
	return date('Ymd\THis', $unix);
}

$filename = str_replace(' ', '_', $data->summary).'.ics';

$nl = "\r\n";

$file = "BEGIN:VCALENDAR".$nl;
$file.= "VERSION:2.0".$nl;
$file.= "PRODID:-//AM.WD/WebCal//NONSGML v1.0//EN".$nl;
$file.= "CALSCALE:GREGORIAN".$nl;
$file.= "BEGIN:VEVENT".$nl;
$file.= "TZID:".date('e').$nl;
$file.= "DTEND:".dateTime($data->end).$nl;
$file.= "UID:".uniqid().$nl;
$file.= "ORGANIZER;CN=\"".escapeSequence($data->name)."\":MAILTO:".escapeSequence($data->email).$nl;
$file.= "DTSTAMP:".dateTime(time()).$nl;
$file.= "LOCATION:".escapeSequence($data->location).$nl;
$file.= "DESCRIPTION:".escapeSequence($data->description).$nl;
$file.= "URL;VALUE=URI:".escapeSequence($data->url).$nl;
$file.= "SUMMARY:".escapeSequence($data->summary).$nl;
$file.= "DTSTART:".dateTime($data->begin).$nl;
$file.= "END:VEVENT".$nl;
$file.= "END:VCALENDAR";

header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: attachment; filename='.$filename);
echo $file;
flush();
?>