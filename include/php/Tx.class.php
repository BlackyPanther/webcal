<?php

/**
 * Tx.class.php
 *
 * @author Andreas Mueller <webmaster@am-wd.de>
 * @version 1.0-20150528
 *
 * @description
 * Translation class. As TxLib for C# and .NET
 * All rights reseved on Unclassified!
 * Author of original is known.
 * To build translation files, use TxEditor from unclassified.software
 **/
namespace AMWD;

@error_reporting(0);

class Tx {

	// Private members for datamanagement
	private static $languages = array();
	private static $defaultLanguage = 'en';
	private static $language = '';

	// Load all xml and txd files from a directory and parse them
	// @param $dir: absolute path to directory
	// @return: true on success or false if directory does not exist
	public static function LoadDirectory($dir) {
		if (!is_dir($dir)) {
			return false;
		}

		$d = opendir($dir);
		while ($file = readdir($d)) {
			if (preg_match("/(\.(([a-z]{2})([-][a-z]{2})?))?\.(?:xml|txd)$/", $file)) {
				self::LoadFromXmlFile($dir.$file);
			}
		}
		closedir($d);

		return true;
	}

	// Load dictionary from specific file.
	// This may override existing entries!
	// @param $file: absolute path to file
	public static function LoadFromXmlFile($file) {
		switch (substr($file, -3)) {
			case 'txd':
				self::parseTxd($file);
				break;
			case 'xml':
				self::parseXml($file);
				break;
			default:
				// currently no error detection
				break;
		}
	}

	// parse a txd file into dictionary
	// @param $file: absolute path to txd file
	private static function parseTxd($file) {
		$xml = new \XMLReader();
		$xml->open($file);

		// save current culture
		$culture = '';

		// read step by step the whole file
		while ($xml->read()) {
			// we're only interested into elements
			if ($xml->nodeType == \XMLReader::ELEMENT) {
				// we have reached a new culture
				if ($xml->name == 'culture') {
					// set it to current culture
					$culture = $xml->getAttribute('name');
					// check if it has the primary attribute
					// if so, it will be our default language
					if ($xml->getAttribute('primary')) {
						self::$defaultLanguage = $culture;
					}
				}

				// we have our text tags => these are our translations!
				if ($xml->name == 'text') {
					$key = $xml->getAttribute('key');
					self::$languages[$culture][$key] = $xml->readString();
				}
			}
		}

		$xml->close();
	}

	// parse a xml file into dictionary
	// culture name has to be in filename
	// @param $file: absolute path to xml file
	private static function parseXml($file) {
		// extract culture from filename
		// e.g. tx.de.xml, tx.en.xml
		$tmp = explode('.', $file);
		$culture = $tmp[count($tmp)-2];
		
		$xml = new \XMLReader();
		$xml->open($file);
		
		// and here we go!
		// same procedure as on parseTxd
		// BUT culture element is missing
		while ($xml->read()) {
			if ($xml->nodeType == \XMLReader::ELEMENT) {
				if ($xml->name == 'translation') {
					if ($xml->getAttribute('primary')) {
						self::$defaultLanguage = $culture;
					}
				}
				
				if ($xml->name == 'text') {
					$key = $xml->getAttribute('key');
					self::$languages[$culture][$key] = $xml->readString();
				}
			}
		}
		
		$xml->close();
	}

	// T is our translation function
	// @param $key: the key to match our translation
	// @param $placeholder: associative array with placeholders and their text
	//                      placeholders can be set with { }
	// @return: translation of the $key
	public static function T($key, $placeholder = array()) {
		// do we have a language set ?
		$lang = self::$language;
		// if not, try to get it from our server
		if (empty($lang))
			$lang = empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? '' : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

		// validate, if its present
		$found = false;
		foreach (self::$languages as $code => $dict) {
			if ($code == $lang)
				$found = true;
		}

		// fallback => use default language
		if (!$found)
			$lang = self::$defaultLanguage;

		// so do we have that key in our dictionary ?!?
		$text = empty(self::$languages[$lang][$key]) ? '['.$key.']' : self::$languages[$lang][$key];

		// replace our placeholders by their text
		foreach ($placeholder as $key => $value) {
			$text = str_replace('{'.$key.'}', $value, $text);
		}

		// yeah... done!
		return $text;
	}

	// possibillity to change default language
	// @param $language: culturecode of the language
	public static function SetDefaultLanguage($language) {
		if (strlen($language) == 2) {
			self::$defaultLanguage = $language;
		}
	}

	// change language, e.g. caused by a userinput
	// @param $language: culturecode of the language
	public static function SetLanguage($language) {
		if (strlen($language) == 2) {
			self::$language = $language;
		}
	}
}

?>