<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-05-24
// Description:
// helper functions
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
//defined('main') || die('<strong>Error:</strong> unauthorized access');

function randomString($length = 8, $pattern = '') {
	if (empty($pattern)) $pattern = md5(time());
	
	$str = '';
	for ($i = 0; $i < $length; $i++) {
		$pos = mt_rand() % strlen($pattern);
		$str .= $pattern[$pos];
	}
	
	return $str;
}

// function to create an alertbox on website
function jqAlert($text, $icon = 'info', $type = 'info') {
	$state = (!empty($type) && $type == 'error') ? 'error' : 'highlight';
	$icon = !empty($icon) ? '<span class="ui-icon ui-icon-'.$icon.'" style="float: left; margin-right: .2em;"></span>' : '';
	
	return '
	<div style="text-align: center">
		<div class="ui-widget" style="display: inline-block; margin: .3em;">
			<div class="ui-state-'.$state.' ui-corner-all" style="display: table-cell; padding: .3em .8em .3em .4em;">
				<p>
					'.$icon.' '.$text.'
				</p>
			</div>
		</div>
	</div>
	';
}














?>