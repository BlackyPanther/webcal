<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-13
// Description:
// Check which page to load
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
use AMWD\Tx as Tx;

$layout['javascript'][] = URL.'include/js/calendar.js';

if ((isset($_SESSION['LoginState']) && $_SESSION['LoginState']) || $_SESSION['PublicAccess']) {
	if ($_SESSION['PublicAccess'] && !(isset($_SESSION['LoginState']) && $_SESSION['LoginState'])) {
		$ckPublic = 'checked="checked" ';
		$layout['side'] = '<div id="sideCal"></div>';
	} else if (isset($_SESSION['LoginState']) && $_SESSION['LoginState'] && empty($_GET['calendar'])) {
		$ckPrivate = ($_SESSION['Calendar'] == 'private') ? 'checked="checked" ' : '';
		$ckPublic = ($_SESSION['Calendar'] == 'public') ? 'checked="checked" ' : '';
	} else {
		$ckPrivate = ($_GET['calendar'] == 'private') ? 'checked="checked" ' : '';
		$ckPublic = ($_GET['calendar'] == 'public') ? 'checked="checked" ' : '';
	}
	
	if (isset($_SESSION['LoginState']) && $_SESSION['LoginState']) {
		$layout['side'] = '
		<div id="calSelect">
			<input type="radio" id="calPrivate" name="calSelect" value="private" '.$ckPrivate.'/><label for="calPrivate">'.Tx::T('WebCal.Side.View.Private').'</label>
			<input type="radio" id="calPublic" name="calSelect" value="public" '.$ckPublic.'/><label for="calPublic">'.Tx::T('WebCal.Side.View.Public').'</label>
		</div>
		<hr />
		<div id="sideCal"></div>
		<hr />
		<ul id="menu">
			<li><a href="#" id="createNewDate"><span class="ui-icon ui-icon-clock"></span>'.Tx::T('WebCal.Side.Action.NewDate').'</a></li>
			<li><a href="#" id="writeMail"><span class="ui-icon ui-icon-mail-closed"></span>'.Tx::T('WebCal.Side.Action.Email').'</a></li>
			<li><a href="#" class="showUserSettings"><span class="ui-icon ui-icon-gear"></span>'.Tx::T('WebCal.Side.Action.Settings').'</a></li>
			';
		if (isset($_SESSION['Admin']) && $_SESSION['Admin']) {
			$layout['side'] .= '<li><a href="'.URL.'admin.php"><span class="ui-icon ui-icon-wrench"></span>'.Tx::T('WebCal.Side.Action.Admin').'</a></li>';
		}
		$layout['side'] .= '
		</ul>
		';
	}
	
	include DIR.'include/default/calendar.php';
} else {
	if (isset($_GET['eID']) && !empty($_GET['eID'])) {
		header('Location: '.URL.'admin.php?p=login&eID='.$_GET['eID']);
		exit;
	}
	header('Location: '.URL.'admin.php?p=login');
}

?>