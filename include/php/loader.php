<?php
/*
// =============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-05-28
// Description:
// Loader - load default settings and layout
// =============================================================================

// =============================================================================
// Changelog:
//
// Date       | Change
// -----------+-----------------------------------------------------------------
//            |
// =============================================================================
*/
defined('main') || die('<strong>Error:</strong> unauthorized access');
use AMWD\Tx as Tx;

// load some config
$sql->open();
$res = $sql->query("SELECT * FROM ".$config['pfx']."settings WHERE settingsid = 1");
if ($sql->num_rows($res) == 1) {
	$row = $sql->fetch_object($res);
	$sql->close();
	
	$layout['headStyle'] = ($row->showClock == 1) ? '' : '#headMain { width: 1000px; } #headClock { display: none; }';
	$layout['title'] = empty($row->title) ? Tx::T('WebCal.Title') : $row->title;
	$layout['slogan'] = (empty($row->slogan) && empty($row->title)) ? Tx::T('WebCal.Slogan') : $row->slogan;
	
	$layout['company'] = $row->company;
	$layout['companyLink'] = $row->companyLink;
	
	if ($row->loading == 1)
		$layout['javascript'][] = URL.'include/js/loadingShade.js';
	
	require_once DIR.'include/'.(defined('admin') ? 'admin/' : 'php/').'pageload.php';
} else {
	$sql->close();
	$layout['content'] = '<strong>Error:</strong> Database Error';
}

require_once DIR.'include/php/layout.php';

?>