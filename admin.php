<?php
/*
// ==============================================================================
// Author: Andreas Mueller <webmaster@am-wd.de>
// Created: 2015-03-23
// Description:
// Main File. Initialise whole admin website.
// ==============================================================================

// ==============================================================================
// Changelog:
//
// Date       | Change
// -----------+------------------------------------------------------------------
//            |
// ==============================================================================
*/

@error_reporting(E_ALL ^ E_NOTICE);
@ini_set('display_errors', 'on');

// prepare vars
$url = str_replace($_SERVER['DOCUMENT_ROOT'], 'http://'.$_SERVER['HTTP_HOST'], __DIR__);
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") $url = str_replace("http", "https", $url);

// define some vars
define('main', true);
define('admin', true);
define('DIR', __DIR__.'/');
define('URL', $url.'/');

// load rest
require_once DIR.'include/php/header.php';
require_once DIR.'include/php/loader.php';

?>