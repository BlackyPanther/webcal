# WebCal
--------

This project is a refactored version of my first WebCal.
Page reloads are replaced by ajax requests to minimize traffic and try to get a smooth handling.

## Features

- Public and Private calendar
  * private calendar per user
  * public calendar for all users
- Circular Mail to all (or part of) users
- Every user has its own settings
  * background color for appointment
  * font color for appointment
  * individual begin and end of day
  * view and selected calendar after login
- Multilingual via [TxEditor](http://unclassified.software/de/source/txtranslation)
- Setup file: [webcal-setup.php](https://am-wd.de/download/webcal-setup.php)

## Requirements
- PHP >= 5.4
- [SQL Class](https://am-wd.de/index.php?p=projects#sql-class)
- JavaScript enabled browser

## Screenshots

More at [am-wd.de](https://am-wd.de/index.php?p=projects#webcal)

![Overview](https://dev.am-wd.de/pics/webcal/05.png)

![Saved Settings](https://dev.am-wd.de/pics/webcal/09.png)

-----

### LICENSE
My scripts are published under [MIT License](https://am-wd.de/index.php?p=about#license).
